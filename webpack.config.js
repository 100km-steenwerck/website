const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

const options = {
  entry: {
    main: [
      './assets/js/main.js',
      './assets/css/main.css',
    ],
  },
  resolve: {
    extensions: ['.js'],
  },
  output: {
    clean: true,
    filename: '[name].js',
    chunkFilename: '[name].[contenthash].js',
    path: path.resolve('./website/static'),
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: path.resolve('./assets/img'), to: 'img' },
        { from: path.resolve('./assets/favicon'), to: '' },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[name].[contenthash].css',
    }),
    new SVGSpritemapPlugin(
      ['assets/icons/*.svg'],
      {
        input: {
          options: {
            realpath: true,
          },
        },
        output: {
          filename: 'icons-sprite.svg',
        },
        sprite: {
          prefix: '',
          generate: { title: false },
        },
      },
    ),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', { targets: 'defaults' }],
            ],
          },
        },
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                plugins: [
                  'postcss-import',
                  ['tailwindcss/nesting', 'postcss-nesting'],
                  'tailwindcss',
                  'autoprefixer',
                ],
              },
            },
          },
        ],
      },
      {
        test: /\.(ttf|woff|woff2)$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name][ext]',
        },
      },
    ],
  },
};

const webpackConfig = (environment, argv) => {
  const isProduction = argv.mode === 'production';

  options.mode = isProduction ? 'production' : 'development';

  if (isProduction) {
    options.optimization = {
      minimizer: [
        '...',
        new CssMinimizerPlugin(),
      ],
    };
  } else {
    options.devServer = {
      client: {
        overlay: {
          errors: true,
          warnings: false,
        },
        reconnect: false,
      },
      devMiddleware: {
        index: false,
        publicPath: '/static',
        writeToDisk: true,
      },
      proxy: [
        {
          context: () => true,
          target: 'http://127.0.0.1:8000',
        },
      ],
      static: false,
    };

    options.devtool = 'inline-source-map';
    options.watchOptions = {
      aggregateTimeout: 1000,
    };

    options.stats = {
      builtAt: false,
      chunks: false,
      hash: false,
      colors: true,
      reasons: false,
      version: false,
      modules: false,
      performance: false,
      children: false,
      assets: false,
    };
  }

  return options;
};

module.exports = webpackConfig;
