from django.forms import widgets


class NativeDateInput(widgets.DateInput):
    input_type = "date"

    def __init__(self, attrs=None):
        super().__init__(attrs)
        self.format = "%Y-%m-%d"


class NativeDateTimeInput(widgets.DateTimeInput):
    input_type = "datetime-local"

    def __init__(self, attrs=None):
        super().__init__(attrs)
        self.format = "%Y-%m-%dT%H:%M"
