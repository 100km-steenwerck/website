from django import forms
from django.core.exceptions import PermissionDenied
from django.template.defaultfilters import filesizeformat
from django.template.loader import render_to_string

from ..models import Registration, RegistrationOption
from ..validators import FileSizeValidator, FileTypeValidator
from ..widgets import NativeDateInput
from .mixins import TailwindTapeformMixin

#: Les types de fichiers autorisés pour le certificat médical.
MEDICAL_CERTIFICATE_ALLOWED_TYPES = ["application/pdf", "image/jpeg"]

#: La taille maximale du certificat médical.
MEDICAL_CERTIFICATE_MAX_SIZE = 5000000  # 5 MB


class RegistrationOptionMultipleChoiceField(forms.ModelMultipleChoiceField):
    """
    Champ de formulaire pour sélectionner les options d'une inscription.
    """

    widget = forms.CheckboxSelectMultiple

    def __init__(self, **kwargs):
        super().__init__(RegistrationOption.objects.all(), **kwargs)

    def label_from_instance(self, obj):
        return render_to_string(
            "forms/fields/registration_option_label.html",
            context={"object": obj},
        )


class BaseRegistrationForm(TailwindTapeformMixin, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        if instance := kwargs.get("instance"):
            if not instance.status == Registration.Status.DRAFT:
                raise PermissionDenied(
                    "L'inscription n'est plus à l'état de brouillon."
                )
        super().__init__(*args, **kwargs)


class RegistrationIdentityForm(BaseRegistrationForm):
    """
    Formulaire utilisé lors de l'inscription à une course pour l'étape
    « Informations personnelles ».

    C'est l'étape initiale qui peut amener à la création d'un nouvel objet,
    ou permettre la modification d'un objet existant s'il a comme statut
    `DRAFT`.
    """

    class Meta:
        model = Registration
        fields = [
            "sex",
            "first_name",
            "last_name",
            "birthdate",
            "street_address",
            "zip_code",
            "city",
            "country",
            "email",
            "phone",
            "ffa_license_number",
            "medical_certificate",
            "club",
        ]
        widgets = {
            "birthdate": NativeDateInput,
            "street_address": forms.Textarea(attrs={"rows": 2}),
        }

    layout_template = "forms/layouts/registration_identity.html"

    # Champs

    email = forms.EmailField(label="Adresse mail", required=True)

    medical_certificate = forms.FileField(
        label="Justificatif médical",
        help_text=(
            "Votre justificatif doit être au format JPEG ou PDF et ne doit pas "
            "dépasser %(max_size)s. Si vous ne l'avez pas encore, il vous "
            "faudra nous le faire parvenir dès que possible par mail."
        )
        % {
            "max_size": filesizeformat(MEDICAL_CERTIFICATE_MAX_SIZE),
        },
        required=False,
        validators=[
            FileTypeValidator(allowed_types=MEDICAL_CERTIFICATE_ALLOWED_TYPES),
            FileSizeValidator(max_size=MEDICAL_CERTIFICATE_MAX_SIZE),
        ],
        widget=forms.ClearableFileInput(
            attrs={"accept": ",".join(MEDICAL_CERTIFICATE_ALLOWED_TYPES)},
        ),
    )


class RegistrationOptionsForm(BaseRegistrationForm):
    """
    Formulaire utilisé lors de l'inscription à une course pour l'étape
    « Options d'inscriptions ».

    Cette étape est à proposer que si l'édition de la course définis des options
    d'inscriptions, et l'objet à modifier doit avoir comme statut `DRAFT`.
    """

    class Meta:
        model = Registration
        fields = ["options"]

    field_label_css_class = "inline-block"
    widget_template_overrides = {
        "options": "forms/widgets/registration_options.html",
    }

    # Champs

    options = RegistrationOptionMultipleChoiceField(
        label=(
            "Vous pouvez choisir de souscrire en plus aux options "
            "suivantes :"
        ),
        required=False,
    )

    # Méthodes

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Filtre les options pour ne proposer que celles définies par
        # l'édition de la course de cette inscription
        options = self.fields["options"]
        options.queryset = options.queryset.filter(
            race_edition=self.instance.race_edition_id
        )

    def _save_m2m(self):
        super()._save_m2m()

        # Mets à jour le montant total de l'inscription lorsque les relations
        # "many-to-many" (utilisées par les options) sont enregistrées
        self.instance.update_total_price()


class RegistrationConfirmationForm(BaseRegistrationForm):
    """
    Formulaire utilisé lors de l'inscription à une course pour l'étape
    « Confirmation ».

    C'est la dernière étape qui récapitule et demande à la personne d'accepter
    le règlement et les conditions de vente pour confirmer l'inscription. Le
    statut de l'objet passe alors à `NEW`.
    """

    class Meta:
        model = Registration
        fields = ["comment"]
        labels = {
            "comment": (
                "Vous pouvez aussi nous laisser un commentaire pour toutes "
                "précisions sur votre inscription :"
            ),
        }
        widgets = {
            "comment": forms.Textarea(attrs={"rows": 4}),
        }

    field_label_css_class = "inline-block"
    layout_template = "forms/layouts/registration_confirmation.html"

    # Champs

    accept_rules = forms.BooleanField(
        label="J'ai lu et j'accepte le règlement et les conditions de vente",
    )
