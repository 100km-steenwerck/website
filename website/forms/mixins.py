from django import forms

from tapeforms.mixins import TapeformMixin


class TailwindTapeformMixin(TapeformMixin):
    layout_template = "forms/layouts/default.html"

    field_template = "forms/fields/default.html"
    field_container_css_class = "mb-4"
    field_label_css_class = "inline-block uppercase text-sm font-medium"
    widget_css_class = "block w-full mt-1"
    widget_invalid_css_class = "!border-red focus:ring-red/60"

    widget_template_overrides = {
        forms.CheckboxSelectMultiple: "forms/widgets/multiple_input.html",
        forms.ClearableFileInput: "forms/widgets/clearable_file_input.html",
        forms.RadioSelect: "forms/widgets/multiple_input.html",
    }

    def get_widget_css_class(self, field_name, field):
        if field.widget.__class__ in [
            forms.CheckboxInput,
            forms.CheckboxSelectMultiple,
            forms.ClearableFileInput,
            forms.RadioSelect,
        ]:
            return ""

        return self.widget_css_class
