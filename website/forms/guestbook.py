from django import forms

from ..models import GuestBookEntry
from .mixins import TailwindTapeformMixin


class GuestBookEntryForm(TailwindTapeformMixin, forms.ModelForm):
    class Meta:
        model = GuestBookEntry
        fields = ["name", "email", "message"]
        help_texts = {
            "email": (
                "Votre adresse mail ne sera utilisée que pour vous recontacter "
                "si besoin."
            )
        }
