from django import forms

from wagtail.contrib.forms.forms import BaseForm, FormBuilder

from .mixins import TailwindTapeformMixin


class BaseVolunteerForm(TailwindTapeformMixin, BaseForm):
    field_template_overrides = {
        forms.MultipleChoiceField: (
            "forms/fields/expandable_multiple_choice.html"
        ),
    }


class VolunteerFormBuilder(FormBuilder):
    def get_form_class(self):
        return type("VolunteerForm", (BaseVolunteerForm,), self.formfields)
