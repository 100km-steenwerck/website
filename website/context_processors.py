from wagtail.models import Site

from .models import SiteSettings


def root_page(request):
    return {"root_page": Site.find_for_request(request).root_page}


def site_settings(request):
    return {"site_settings": SiteSettings.for_request(request)}
