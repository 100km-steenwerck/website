# Generated by Django 4.2.7 on 2023-11-20 14:37

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import wagtail.contrib.routable_page.models
import wagtail.fields


class Migration(migrations.Migration):
    dependencies = [
        ('wagtailcore', '0089_log_entry_data_json_null_to_object'),
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GuestBookEntry',
            fields=[
                (
                    'id',
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                ('name', models.CharField(max_length=150, verbose_name='nom')),
                (
                    'email',
                    models.EmailField(
                        blank=True, max_length=254, verbose_name='adresse mail'
                    ),
                ),
                ('message', models.TextField(verbose_name='message')),
                (
                    'date',
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name='date'
                    ),
                ),
                (
                    'is_visible',
                    models.BooleanField(default=True, verbose_name='visible'),
                ),
            ],
            options={
                'verbose_name': "message du livre d'or",
                'verbose_name_plural': "messages du livre d'or",
                'ordering': ['-date'],
            },
        ),
        migrations.CreateModel(
            name='GuestBookPage',
            fields=[
                (
                    'page_ptr',
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to='wagtailcore.page',
                    ),
                ),
                (
                    'introduction',
                    wagtail.fields.RichTextField(
                        verbose_name="texte d'introduction"
                    ),
                ),
                (
                    'per_page',
                    models.PositiveSmallIntegerField(
                        default=15, verbose_name='messages par page'
                    ),
                ),
            ],
            options={
                'verbose_name': "livre d'or",
                'verbose_name_plural': "livres d'or",
            },
            bases=(
                wagtail.contrib.routable_page.models.RoutablePageMixin,
                'wagtailcore.page',
            ),
        ),
    ]
