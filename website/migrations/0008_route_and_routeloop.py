# Generated by Django 4.2.8 on 2023-12-29 12:27

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import dynamic_filenames
import modelcluster.fields
import wagtail.blocks
import wagtail.contrib.typed_table_block.blocks
import wagtail.fields
import wagtail.images.blocks
import website.blocks


class Migration(migrations.Migration):
    dependencies = [
        ('website', '0007_indexpage_and_introduction_mixin'),
    ]

    operations = [
        migrations.CreateModel(
            name='Route',
            fields=[
                (
                    'id',
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'name',
                    models.CharField(
                        max_length=150, unique=True, verbose_name='nom'
                    ),
                ),
            ],
            options={
                'verbose_name': 'parcours',
                'verbose_name_plural': 'parcours',
            },
        ),
        migrations.CreateModel(
            name='RouteLoop',
            fields=[
                (
                    'id',
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'sort_order',
                    models.IntegerField(blank=True, editable=False, null=True),
                ),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                (
                    'color',
                    models.CharField(
                        choices=[
                            ('#f44336', 'Rouge'),
                            ('#e91e63', 'Rose'),
                            ('#9c27b0', 'Violet'),
                            ('#3f51b5', 'Indigo'),
                            ('#2196f3', 'Bleu'),
                            ('#00bcd4', 'Cyan'),
                            ('#4caf50', 'Vert'),
                            ('#ffeb3b', 'Jaune'),
                            ('#ffc107', 'Ambre'),
                            ('#ff9800', 'Orange'),
                            ('#795548', 'Marron'),
                            ('#9e9e9e', 'Gris'),
                        ],
                        max_length=7,
                        verbose_name='couleur',
                    ),
                ),
                (
                    'distance',
                    models.DecimalField(
                        decimal_places=3,
                        max_digits=6,
                        verbose_name='distance (en km)',
                    ),
                ),
                (
                    'gpx_file',
                    models.FileField(
                        upload_to=dynamic_filenames.FilePattern(
                            filename_pattern='routes/{instance.route.pk}/{name:slug}{ext}'
                        ),
                        validators=[
                            django.core.validators.FileExtensionValidator(
                                allowed_extensions=['gpx'],
                                message='Format de fichier non autorisé.',
                            ),
                        ],
                        verbose_name='fichier GPX',
                    ),
                ),
                (
                    'route',
                    modelcluster.fields.ParentalKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='loops',
                        related_query_name='loop',
                        to='website.route',
                    ),
                ),
            ],
            options={
                'verbose_name': 'boucle',
                'verbose_name_plural': 'boucles',
                'ordering': ['sort_order'],
                'unique_together': {('route', 'name')},
            },
        ),
    ]
