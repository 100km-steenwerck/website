import re
from urllib.parse import urljoin

from django.conf import settings
from django.utils.encoding import iri_to_uri

from authlib.integrations.requests_client import OAuth2Session
from requests.exceptions import RequestException

INVALID_CHARACTERS_RE = re.compile(r"[^a-zàâæçéèêëîïôœùûüÿ\-' ]", re.IGNORECASE)


class APIResponseError(RequestException):
    def __init__(self, error_list: list, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.error_list = error_list


class HelloAssoOAuth2Session(OAuth2Session):
    def request(self, method, url, withhold_token=False, auth=None, **kwargs):
        # Demande automatiquement un token d'accès s'il n'y en a pas encore
        if not withhold_token and auth is None and not self.token:
            self.token = self.fetch_token(grant_type="client_credentials")
        return super().request(method, url, auth=auth, **kwargs)


class BaseClient:
    #: L'URL de base pour atteindre l'API.
    #: (requis, à définir par les sous-classes)
    base_url = None

    #: L'URL pour obtenir un token d'accès à l'API.
    #: (requis, à définir par les sous-classes)
    token_endpoint = None

    #: Le slug de l'organisation avec laquelle l'accès à l'API se fait.
    organization_slug = "100-km-a-pied-de-steenwerck"

    def __init__(self):
        self.session = HelloAssoOAuth2Session(
            client_id=self.client_id,
            client_secret=self.client_secret,
            token_endpoint=self.token_endpoint,
            token_endpoint_auth_method="client_secret_post",
        )

    @property
    def client_id(self):
        return settings.HELLOASSO_API_CLIENT_ID

    @property
    def client_secret(self):
        return settings.HELLOASSO_API_CLIENT_SECRET

    def get(self, url, **kwargs):
        return self.session.get(self.get_url(url), **kwargs)

    def post(self, url, **kwargs):
        return self.session.post(self.get_url(url), **kwargs)

    def init_checkout_intent(
        self,
        amount: int,
        item_name: str,
        back_url: str,
        error_url: str,
        return_url: str,
        payer_first_name: str,
        payer_last_name: str,
        payer_email: str,
        request=None,
    ):
        """Initialise une demande d'encaissement.

        :param amout: Montant total (TTC) du paiement en centimes.
        :param item_name: Description de l’achat.
        :param back_url: L'URL de retour (page précédente) si la personne
                         souhaite modifier son panier avant de payer.
        :param error_url: L'URL de retour en cas d’erreur technique.
        :param return_url: L'URL de retour après le paiement.
        :param payer_first_name: Le prénom du payeur.
        :param payer_last_name: Le nom du payeur.
        :param payer_email: L'adresse mail du payeur.
        :param request: La requête à l'origine de cette demande de paiement.
        :return: Un 2-tuple (checkoutIntentId, redirectUrl).
        """
        response = self.post(
            "checkout-intents",
            json={
                "totalAmount": amount,
                "initialAmount": amount,
                "itemName": item_name,
                "backUrl": self.build_absolute_uri(back_url, request),
                "errorUrl": self.build_absolute_uri(error_url, request),
                "returnUrl": self.build_absolute_uri(return_url, request),
                "containsDonation": False,
                "payer": {
                    "firstName": self.sanitize_string(payer_first_name),
                    "lastName": self.sanitize_string(payer_last_name),
                    "email": payer_email,
                },
            },
        )
        data = self.parse_json_response(response)
        return data["id"], data["redirectUrl"]

    # Helpers

    def get_url(self, location):
        """
        Construit et retourne la forme absolue de l'URI `location` relatif à
        l'organisation définit par ce client.
        """
        return urljoin(
            self.base_url,
            f"organizations/{self.organization_slug}/{location}",
        )

    def build_absolute_uri(self, location, request=None):
        """
        Construit et retourne la forme absolue de l'URI `location` en fonction
        de la requête ou du paramètre `BASE_URL`.
        """
        if request is not None:
            return request.build_absolute_uri(location)
        return iri_to_uri(urljoin(settings.BASE_URL, location))

    def parse_json_response(self, response):
        """
        Récupère la réponse depuis `response` au format JSON en vérifiant qu'il
        n'y ait pas d'erreur.
        """
        data = response.json()
        if "errors" not in data:
            return data
        raise APIResponseError(data["errors"], response=response)

    def sanitize_string(self, value):
        """
        Formate une chaîne de caractères pour ne garder que ceux autorisés par
        l'API de HelloAsso.
        """
        return INVALID_CHARACTERS_RE.sub("", value)


class ProductionClient(BaseClient):
    base_url = "https://api.helloasso.com/v5/"
    token_endpoint = "https://api.helloasso.com/oauth2/token"


class SandboxClient(BaseClient):
    base_url = "https://api.helloasso-sandbox.com/v5/"
    token_endpoint = "https://api.helloasso-sandbox.com/oauth2/token"

    def build_absolute_uri(self, location, request=None):
        # Les URLs à passer en paramètre à l'API HelloAsso nécessitent un accès
        # HTTPS uniquement, on le force si ce n'est pas le cas de la requête
        if request is not None and not request.is_secure():
            return f"https://{request.get_host()}{location:s}"
        return super().build_absolute_uri(location, request)


def get_client():
    client_class = (
        ProductionClient if settings.USE_HELLOASSO_PRODUCTION else SandboxClient
    )
    return client_class()


client = get_client()
