import logging

from django.core.exceptions import ValidationError
from django.template.defaultfilters import filesizeformat
from django.utils.deconstruct import deconstructible

import filetype

logger = logging.getLogger("website.validators")


@deconstructible
class FileSizeValidator:
    message = (
        "La taille du fichier (%(size)s) dépasse celle autorisée "
        "(maximum : %(max_size)s)."
    )
    code = "file_size_too_large"

    def __init__(self, max_size: int = None):
        self.max_size = max_size

    def __call__(self, value):
        if self.max_size is not None and value.size > self.max_size:
            size = filesizeformat(value.size)
            max_size = filesizeformat(self.max_size)

            logger.info(
                "Fichier « %s » trop volumineux (%s / %s)",
                value.name,
                size,
                max_size,
            )
            raise ValidationError(
                self.message
                % {
                    "size": size,
                    "max_size": max_size,
                },
                code=self.code,
            )

    def __eq__(self, other):
        return (
            isinstance(other, FileSizeValidator)
            and self.max_size == other.max_size
        )


@deconstructible
class FileTypeValidator:
    message = (
        "Le format du fichier (%(type)s) n'est pas autorisé "
        "(types autorisés : %(allowed_types)s)."
    )
    code = "invalid_file_type"

    def __init__(self, allowed_types: list = None):
        self.allowed_types = allowed_types

    def __call__(self, value):
        if self.allowed_types is not None:
            kind = filetype.guess(value)

            if not kind or kind.mime not in self.allowed_types:
                mime = kind.mime if kind else "inconnu"
                logger.warning(
                    "Format du fichier « %s » non autorisé (%s)",
                    value.name,
                    mime,
                )
                raise ValidationError(
                    self.message
                    % {
                        "type": mime,
                        "allowed_types": ", ".join(self.allowed_types),
                    },
                    code=self.code,
                )

    def __eq__(self, other):
        return (
            isinstance(other, FileTypeValidator)
            and self.allowed_types == other.allowed_types
        )
