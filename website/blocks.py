import json

from django import forms
from django.core.exceptions import ValidationError
from django.db import models
from django.template.defaultfilters import linebreaks_filter
from django.utils.formats import date_format, time_format
from django.utils.functional import cached_property
from django.utils.text import slugify

from wagtail import blocks
from wagtail.blocks.field_block import FieldBlockAdapter
from wagtail.blocks.struct_block import (
    StructBlockAdapter,
    StructBlockValidationError,
)
from wagtail.contrib.typed_table_block.blocks import (
    TypedTableBlock,
    TypedTableBlockAdapter,
)
from wagtail.coreutils import resolve_model_string
from wagtail.images.blocks import ImageChooserBlock
from wagtail.telepath import register

PARAGRAPH_FEATURES = [
    "bold",
    "italic",
    "ol",
    "ul",
    "hr",
    "link",
    "document-link",
]


class NoCommentFieldBlockAdapter(FieldBlockAdapter):
    def js_args(self, block):
        js_args = super().js_args(block)
        # Force la suppression du bouton pour ajouter un commentaire, par
        # défaut cela se passe au niveau du widget du champ et ne tient
        # pas compte de `WAGTAILADMIN_COMMENTS_ENABLED`
        js_args[2]["showAddCommentButton"] = False
        return js_args


register(NoCommentFieldBlockAdapter(), blocks.FieldBlock)


class BaseSelectChooserBlock(blocks.ChooserBlock):
    """
    Abstract class for fields that implement a chooser interface for a
    model with a Select widget.
    """

    @cached_property
    def field(self):
        return forms.ModelChoiceField(
            queryset=self.get_queryset(),
            widget=self.widget,
            required=self._required,
            validators=self._validators,
            help_text=self._help_text,
        )

    @cached_property
    def widget(self):
        return forms.Select()

    def value_from_form(self, value):
        if value == "":
            return None
        return super().value_from_form(value)

    def get_form_state(self, value):
        return blocks.FieldBlock.get_form_state(self, value)

    def get_queryset(self):
        """Return the queryset to use for the field."""
        return self.model_class.objects.all()


class RouteChooserBlock(BaseSelectChooserBlock):
    class Meta:
        label = "Parcours"
        icon = "route"

    @cached_property
    def target_model(self):
        return resolve_model_string("website.Route")


# LIEN
# ------------------------------------------------------------------------------


class LinkTargetStructValue(blocks.StructValue):
    @cached_property
    def url(self):
        if block_type := self.get("type"):
            value = self.get(block_type)
            if block_type == "page":
                return value.url
            return value
        return ""


class LinkTargetBlock(blocks.StructBlock):
    class Meta:
        icon = "link"
        label = "Lien"
        value_class = LinkTargetStructValue
        form_template = "admin/forms/link_target_block.html"

    page = blocks.PageChooserBlock(required=False, label="Page")
    url = blocks.URLBlock(required=False, label="Lien externe")
    anchor = blocks.CharBlock(required=False, label="Lien d'ancrage")

    def __init__(self, local_blocks=None, required=True, **kwargs):
        super().__init__(local_blocks=local_blocks, **kwargs)

        self.meta.required = required

        # Retrieve available block types from the defined blocks
        self.block_types = list(self.child_blocks.keys())

        # Construct dynamically the choice block and append it
        type_block = blocks.ChoiceBlock(
            choices=[
                (name, self.child_blocks[name].label)
                for name in self.block_types
            ]
        )
        type_block.set_name("type")

        self.child_blocks["type"] = type_block

    @property
    def required(self):
        return self.meta.required

    def clean(self, value):
        # Build up a list of (name, value) tuples to be passed to the
        # StructValue constructor
        result = []

        errors = {}

        if block_type := value.get("type"):
            result.append(("type", block_type))

            if block_value := value.get(block_type):
                try:
                    result.append(
                        (
                            block_type,
                            self.child_blocks[block_type].clean(block_value),
                        )
                    )
                except ValidationError as e:
                    errors[block_type] = e
            else:
                errors[block_type] = ValidationError(
                    "Champ requis.", code="required"
                )
        elif self.required:
            errors["type"] = ValidationError("Champ requis.", code="required")

        if errors:
            raise StructBlockValidationError(errors)

        return self._to_struct_value(result)


class LinkTargetBlockAdapter(StructBlockAdapter):
    js_constructor = "website.blocks.LinkTargetBlock"

    def js_args(self, block):
        js_args = super().js_args(block)
        js_args[2]["blockTypes"] = block.block_types
        return js_args

    @cached_property
    def media(self):
        css = super().media._css
        css.setdefault("all", [])
        css["all"].append("admin/css/link-target-block.css")

        js = super().media._js
        js.append("admin/js/link-target-block.js")

        return forms.Media(css=css, js=js)


register(LinkTargetBlockAdapter(), LinkTargetBlock)


# BLOCS DE BASE
# ------------------------------------------------------------------------------


class TitleBlock(blocks.CharBlock):
    class Meta:
        icon = "title"
        label = "Titre"
        classname = "title"
        template = "blocks/title_block.html"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        context["id"] = slugify(value)
        return context


class ParagraphBlock(blocks.RichTextBlock):
    class Meta:
        icon = "pilcrow"
        label = "Paragraphe"
        template = "blocks/paragraph_block.html"

    def __init__(self, **kwargs):
        kwargs.setdefault("features", PARAGRAPH_FEATURES)
        super().__init__(**kwargs)


class ImageBlock(blocks.StructBlock):
    class Meta:
        icon = "image"
        label = "Image"
        template = "blocks/image_block.html"

    image = ImageChooserBlock(label="Image")
    caption = blocks.CharBlock(required=False, label="Légende")
    link = LinkTargetBlock(required=False, label="Lien de l'image")


# TABLEAU
# ------------------------------------------------------------------------------


class TableCellMixin:
    def get_cell_classname(self, value):
        """Retourne les classes CSS à ajouter à la cellule."""
        return ""

    def bind(self, value, **kwargs):
        block = super().bind(value, **kwargs)
        block.cell_classname = self.get_cell_classname(value)
        return block


class DateCellBlock(TableCellMixin, blocks.DateBlock):
    class Meta:
        label = "Date"

    def render_basic(self, value, context=None):
        return date_format(value, "d/m/Y")


class TimeCellBlock(TableCellMixin, blocks.TimeBlock):
    class Meta:
        label = "Heure"

    def render_basic(self, value, context=None):
        return time_format(value, "G\\hi")


class TextCellBlock(TableCellMixin, blocks.TextBlock):
    class Meta:
        label = "Texte"

    def render_basic(self, value, context=None):
        return linebreaks_filter(value)


class HeadcountCellBlock(TableCellMixin, blocks.StructBlock):
    class Meta:
        label = "Effectif"
        template = "blocks/headcount_cell_block.html"
        form_template = "admin/forms/headcount_cell_block.html"

    current = blocks.IntegerBlock(min_value=0, default=0, label="Actuel")
    needed = blocks.IntegerBlock(min_value=0, label="Nécessaire")
    details = blocks.TextBlock(required=False, label="Détails")

    def get_cell_classname(self, value):
        if value["current"] > value["needed"]:
            return "bg-yellow-200"
        if value["current"] < value["needed"]:
            return "bg-red-200"
        if value["needed"]:
            return "bg-green-200"
        return ""


class TableBlock(TypedTableBlock):
    class Meta:
        label = "Tableau"
        template = "blocks/table_block.html"

    date = DateCellBlock()
    time = TimeCellBlock()
    text = TextCellBlock()
    headcount = HeadcountCellBlock()


class TableBlockAdapter(TypedTableBlockAdapter):
    @cached_property
    def media(self):
        css = super().media._css
        css.setdefault("all", [])
        css["all"].append("admin/css/table-block.css")

        return forms.Media(css=css, js=super().media._js)


register(TableBlockAdapter(), TableBlock)


# GRILLES
# ------------------------------------------------------------------------------


class ImagesGridBlock(blocks.StructBlock):
    class Meta:
        icon = "images"
        label = "Grille d'images"

    class Sizes(models.TextChoices):
        LOGO = "logos", "Logos"

    size = blocks.ChoiceBlock(choices=Sizes.choices, label="Taille")
    images = blocks.ListBlock(
        ImageBlock(),
        min_num=1,
        collapsed=True,
        label="Images",
    )

    def get_template(self, value=None, context=None):
        return "blocks/images_grid_block__logos.html"


# COMPOSANTS
# ------------------------------------------------------------------------------


class AlertBlock(blocks.StructBlock):
    class Meta:
        icon = "warning"
        label = "Alerte"
        template = "blocks/alert_block.html"

    message = blocks.RichTextBlock(
        features=["bold", "italic", "link", "document-link"],
        label="Message",
    )
    level = blocks.ChoiceBlock(
        choices=[
            ("info", "Info"),
            ("success", "Succès"),
            ("warning", "Alerte"),
            ("error", "Erreur"),
        ],
        label="Niveau",
    )


class RouteMapBlock(blocks.StructBlock):
    class Meta:
        icon = "route"
        label = "Carte de parcours"
        template = "blocks/route_map_block.html"

    route = RouteChooserBlock()

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        loops = list(value["route"].loops.all())
        context.update(
            {
                "loops": loops,
                "loops_json": json.dumps(
                    [
                        {
                            "pk": loop.pk,
                            "name": loop.name,
                            "color": loop.color,
                            "gpxFile": loop.gpx_file.url,
                        }
                        for loop in loops
                    ]
                ),
            }
        )
        return context


# ENSEMBLES DE BLOCS
# ------------------------------------------------------------------------------


class ContentStreamBlock(blocks.StreamBlock):
    """
    Ensemble des blocs disponibles pour le contenu d'une page.
    """

    class Meta:
        template = "blocks/content_stream_block.html"

    title_block = TitleBlock()
    paragraph_block = ParagraphBlock()
    image_block = ImageBlock()

    table_block = TableBlock()
    images_grid_block = ImagesGridBlock()

    alert_block = AlertBlock()
    route_map_block = RouteMapBlock()
