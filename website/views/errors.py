from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseNotFound,
)
from django.template import loader
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import requires_csrf_token
from django.views.generic.base import ContextMixin, View

from .mixins import PageContextMixin


class BaseErrorView(PageContextMixin, ContextMixin, View):
    template_name = "40x.html"

    #: La classe à utiliser pour la réponse.
    response_class = HttpResponse

    #: Le message par défaut décrivant l'erreur, utilisé dans le cas où
    #: l'exception à l'origine de l'erreur n'en définis pas un.
    default_message = ""

    #: Le message décrivant l'erreur. S'il est définis, celui de l'exception
    #: ne sera pas affiché (ex. pour ne pas donner d'informations sensibles).
    message = None

    @method_decorator(requires_csrf_token)
    def dispatch(self, request, exception=None):
        context = self.get_context_data(
            message=self.get_exception_message(exception),
        )
        template = loader.get_template(self.template_name)
        body = template.render(context, request)
        return self.response_class(body)

    def get_exception_message(self, exception):
        if self.message:
            return self.message
        if not exception:
            return self.default_message
        try:
            message = exception.args[0]
        except (AttributeError, IndexError):
            pass
        else:
            if isinstance(message, str):
                return message
        return self.default_message


bad_request = BaseErrorView.as_view(
    response_class=HttpResponseBadRequest,
    page_title="Mauvaise requête",
    message=(
        "La page ou l'opération demandée ne semble pas correcte. Si vous "
        "estimez que c'est une erreur, nous vous invitons à nous le signaler."
    ),
)

permission_denied = BaseErrorView.as_view(
    response_class=HttpResponseForbidden,
    page_title="Accès refusé",
    default_message=(
        "Vous n'avez pas les autorisations pour accéder à cette page. Si vous "
        "estimez que c'est une erreur, nous vous invitons à nous le signaler."
    ),
)

page_not_found = BaseErrorView.as_view(
    response_class=HttpResponseNotFound,
    page_title="Page introuvable",
    default_message=(
        "La page demandée est introuvable, il est possible qu'elle n'existe "
        "plus ou qu'elle a été remplacée. Si vous ne trouvez pas, nous vous "
        "invitons à nous le signaler."
    ),
)
