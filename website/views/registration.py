import logging

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import SuspiciousOperation
from django.forms import Form
from django.http import Http404, HttpResponseRedirect
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.utils import timezone
from django.utils.functional import cached_property
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.edit import FormMixin, ModelFormMixin, ProcessFormView

from requests.exceptions import HTTPError

from website.forms.registration import (
    RegistrationConfirmationForm,
    RegistrationIdentityForm,
    RegistrationOptionsForm,
)
from website.models import PaymentMeans, Registration, RegistrationPayment

from .mixins import PageContextMixin

logger = logging.getLogger("website.registration")


class StepsHelper:
    def __init__(self, view, step_list):
        self.view = view
        self.cache = {
            step["slug"]: dict(step, index=index)
            for index, step in enumerate(step_list)
        }

    def __iter__(self):
        """Retourne un itérateur sur la liste des étapes."""
        current_index = self.current["index"]
        for step in self.all:
            yield dict(
                step,
                is_current=step["index"] == current_index,
                is_previous=step["index"] < current_index,
                is_next=step["index"] > current_index,
            )

    def __len__(self):
        """Retourne le nombre total d'étapes."""
        return len(self.cache)

    def __getitem__(self, slug):
        """Retourne l'étape avec le slug donné."""
        return self.cache[slug]

    def __contains__(self, slug):
        """Détermine si l'étape avec le slug donné existe."""
        return slug in self.cache

    @cached_property
    def all(self):
        """Retourne la liste de toutes les étapes."""
        return list(self.cache.values())

    @property
    def first(self):
        """Retourne la première étape définie."""
        return self.all[0]

    @property
    def current(self):
        """Retourne l'étape en cours."""
        return self.view.get_current_step()

    @property
    def has_next(self):
        """Détermine s'il existe une étape suivante."""
        return self.get_next() is not None

    @property
    def has_previous(self):
        """Détermine s'il existe une étape précédente."""
        return self.get_previous() is not None

    def get_next(self):
        """Retourne l'étape suivante si elle existe."""
        index = self.current["index"] + 1
        if len(self.all) > index:
            return self.all[index]
        return None

    def get_previous(self):
        """Retourne l'étape précédente si elle existe."""
        index = self.current["index"] - 1
        if index >= 0:
            return self.all[index]
        return None


class RegistrationPageMixin(PageContextMixin):
    page_title = "Inscription"
    seo_title = "S'inscrire à la course %(race)s"

    #: L'objet `RaceEdition` pour lequel l'inscription est à associer
    #: (requis, à définir à l'initialisation).
    race_edition = None

    def get_page_title(self):
        return self.race_edition.race.title

    def get_seo_title(self):
        return self.seo_title % {"race": self.race_edition.race.title}

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page"] = self.race_edition.race
        context["race_edition"] = self.race_edition
        return context


class RegistrationObjectPageMixin(RegistrationPageMixin):
    model = Registration

    @property
    def session_key(self):
        """La clé utilisée pour les données en session de cette inscription."""
        return "edition_registration_%d" % self.race_edition.pk

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)

        # Initialise les données en session de cette inscription
        if self.session_key not in self.request.session:
            self.init_session()

        self.is_session_valid = True
        self.object = self.get_object()

    def get_queryset(self):
        return self.model.objects.all().select_related("race_edition__race")

    def get_object(self):
        if registration_id := self.get_session_data("registration_id"):
            try:
                return self.get_queryset().get(pk=registration_id)
            except self.model.DoesNotExist:
                # L'objet n'existe pas ou plus, il a peut-être été supprimé
                # depuis, on réinitialise la session
                self.is_session_valid = False
                self.init_session()
        return None

    def init_session(self):
        """(Ré)initialise la session pour cette inscription."""
        self.request.session[self.session_key] = {}
        self.request.session.modified = True

    def clear_session(self):
        """Supprime les données en session de cette inscription."""
        del self.request.session[self.session_key]

    def get_session_data(self, key, default=None):
        """Retourne la valeur d'un attribut d'inscription depuis la session."""
        return self.request.session[self.session_key].get(key, default)

    def set_session_data(self, key, value):
        """Stocke en session la valeur d'un attribut d'inscription."""
        self.request.session[self.session_key][key] = value
        self.request.session.modified = True


class RegisterView(
    RegistrationObjectPageMixin,
    ModelFormMixin,
    TemplateResponseMixin,
    ProcessFormView,
):
    """
    Présente et traîte les différentes étapes de l'inscription à une édition
    de course, jusqu'à sa confirmation.
    """

    template_name = "registration/form.html"
    done_template_name = "registration/done.html"

    #: Le nom du paramètre nommé dans l'URL contenant le slug de l'étape.
    #: (requis, à définir à l'initialisation)
    step_url_kwarg = None

    #: Une fonction qui prend comme argument le slug d'une étape d'inscription
    #: et retourne l'URL de cette étape.
    #: (requis, à définir à l'initialisation)
    get_step_url = None

    #: L'URL de la page du paiement de l'inscription en cours.
    #: (requis, à définir à l'initialisation)
    checkout_url = None

    #: L'URL de la page d'annulation de l'inscription en cours.
    #: (requis, à définir à l'initialisation)
    cancel_url = None

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)

        self.steps = StepsHelper(self, self.get_step_list())

    def dispatch(self, request, *args, **kwargs):
        # Redirige vers la première étape si la session n'est plus valable
        if not self.is_session_valid:
            return HttpResponseRedirect(self.steps.first["url"])

        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        step_slug = kwargs.get(self.step_url_kwarg)

        if not step_slug:
            return HttpResponseRedirect(self.steps.current["url"])

        if step_slug == "done":
            return self.done_response()

        if step_slug not in self.steps:
            raise Http404("L'étape d'inscription demandée n'existe pas.")

        # Mets à jour si besoin l'étape en cours avec celle demandée
        if step_slug != self.steps.current["slug"]:
            if self.steps[step_slug]["index"] > self.steps.current["index"] + 1:
                logger.warning(
                    "L'étape '%s' demandée est bien après '%s'",
                    step_slug,
                    self.steps.current["slug"],
                )
                return HttpResponseRedirect(self.steps.current["url"])
            self.set_current_step(step_slug)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        step_slug = kwargs.get(self.step_url_kwarg)

        # Vérifie que l'étape du formulaire correspond avec celle en cours
        if step_slug != self.steps.current["slug"]:
            raise SuspiciousOperation(
                "L'étape demandée ne correspond pas avec celle en cours."
            )

        return super().post(request, *args, **kwargs)

    def done_response(self):
        """
        Vérifie que l'inscription existe et est bien validée avant d'afficher
        la page de fin d'inscription.
        """
        if self.object is None:
            messages.error(
                self.request,
                "Aucune inscription ne semble en cours dans votre session.",
            )
            return HttpResponseRedirect(self.steps.current["url"])

        if self.object.status == Registration.Status.DRAFT:
            messages.error(
                self.request,
                "Votre inscription en cours n'est pas encore terminée.",
            )
            return HttpResponseRedirect(self.steps.current["url"])

        self.send_confirmation_mail()
        self.clear_session()

        return TemplateResponse(
            self.request,
            self.done_template_name,
            self.get_context_data(form=None),
        )

    def get_form_class(self):
        return self.steps.current["form_class"]

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # Définis l'édition de la course pour une nouvelle inscription
        if form.instance.pk is None:
            form.instance.race_edition = self.race_edition
        return form

    def form_valid(self, form):
        self.object = form.save()
        self.set_session_data("registration_id", self.object.pk)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        # Retourne l'URL de l'étape suivante ou celle du paiement
        if next_step := self.steps.get_next():
            return next_step["url"]
        return self.checkout_url

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["cancel_url"] = self.cancel_url
        context["steps"] = self.steps
        return context

    def get_step_list(self):
        """Retourne les étapes sous forme de liste."""
        steps = [
            {
                "slug": "identity",
                "label": "Informations personnelles",
                "form_class": RegistrationIdentityForm,
                "url": self.get_step_url("identity"),
            },
            {
                "slug": "confirmation",
                "label": "Confirmation",
                "form_class": RegistrationConfirmationForm,
                "url": self.get_step_url("confirmation"),
            },
        ]
        if self.race_edition.registration_options.count():
            steps.insert(
                1,
                {
                    "slug": "options",
                    "label": "Options d'inscription",
                    "form_class": RegistrationOptionsForm,
                    "url": self.get_step_url("options"),
                },
            )
        return steps

    def get_current_step(self):
        """Retourne l'étape en cours, ou la première par défaut."""
        if current_step := self.get_session_data("current_step"):
            return self.steps[current_step]
        return self.steps.first

    def set_current_step(self, step_slug):
        """Stocke en session la nouvelle étape en cours."""
        self.set_session_data("current_step", step_slug)

    def send_confirmation_mail(self):
        """Envoi le courriel de confirmation à la personne."""
        subject = (
            "Inscription confirmée pour la course %s" % self.race_edition.race
        )
        body = render_to_string(
            "registration/confirmation_email.txt",
            context={
                "registration": self.object,
                "race_edition": self.race_edition,
            },
        )

        self.object.email_person(
            subject,
            body,
            reply_to=settings.REGISTRATION_REPLY_TO,
        )


class RegistrationCheckOutView(RegistrationObjectPageMixin, View):
    """
    Procéde au paiement de l'inscription en cours ou traite son retour.
    """

    #: Le nom du paramètre nommé dans l'URL contenant l'identifiant du retour.
    #: (requis, à définir à l'initialisation)
    callback_url_kwarg = None

    #: Une fonction qui prend comme argument l'identifiant du retour de paiement
    #: et retourne l'URL de son traitement.
    #: (requis, à définir à l'initialisation)
    get_checkout_url = None

    #: Une fonction qui prend comme argument optionnel le slug d'une étape
    #: d'inscription et retourne l'URL de cette étape.
    #: (requis, à définir à l'initialisation)
    get_register_url = None

    def get(self, request, *args, **kwargs):
        if self.object is None:
            messages.error(
                self.request,
                "Aucune inscription ne semble en cours dans votre session.",
            )
            return HttpResponseRedirect(self.get_register_url())

        callback = kwargs.get(self.callback_url_kwarg)

        if callback and callback not in ["cancel", "error", "return"]:
            raise Http404

        # Récupère le paiement en cours éventuel depuis la session
        self.registration_payment = self.get_registration_payment()

        if not callback:
            if self.registration_payment is None:
                return self.init_response()

            logger.warning(
                "Une demande est déjà en cours pour le paiement #%s",
                self.registration_payment.checkout_id,
            )
            messages.error(
                self.request,
                (
                    "Une demande de paiement est déjà en cours pour votre "
                    "inscription, veuillez la terminer ou l'annuler."
                ),
            )
            return HttpResponseRedirect(self.get_register_url("confirmation"))

        if self.registration_payment is None:
            raise SuspiciousOperation(
                "Impossible de récupérer le paiement associé à l'inscription."
            )

        if callback == "cancel":
            return self.cancel_response()

        # Vérifie que le retour concerne le paiement en cours
        checkout_id = request.GET.get("checkoutIntentId")
        if checkout_id != self.registration_payment.checkout_id:
            logger.warning(
                (
                    "L'argument checkoutIntentId ne correspond pas avec celui "
                    "de la session en cours (%s != %s)"
                ),
                str(checkout_id),
                self.registration_payment.checkout_id,
            )
            raise SuspiciousOperation(
                "Le retour de paiement ne correspond pas à votre inscription "
                "en cours."
            )

        if callback == "error":
            return self.error_response()

        # Le paiement a été accepté ou refusé
        code = request.GET.get("code")
        if code == "succeeded":
            return self.success_response()
        if code == "refused":
            return self.refused_response()

        raise Http404

    def get_registration_payment(self):
        """
        Récupère l'objet `RegistrationCheckout` pour la demande de paiement
        en cours.
        """
        if checkout_id := self.get_session_data("checkout_id"):
            return RegistrationPayment.objects.get(
                registration_id=self.object.id,
                checkout_id=str(checkout_id),
                is_pending=True,
            )
        return None

    def delete_registration_payment(self):
        """
        Supprime l'objet `RegistrationCheckout` pour la demande de paiement
        en cours, et nettoie la session.
        """
        self.registration_payment.delete()
        self.set_session_data("checkout_id", None)

    def init_response(self):
        """
        Une demande de paiement est faite : on initialise la demande avec
        HelloAsso et en base avant de rediriger vers l'URL de paiement.
        """
        from website.helloasso_api import APIResponseError, client

        try:
            checkout_id, redirect_url = client.init_checkout_intent(
                int(self.object.total_price * 100),
                "Frais d'inscription pour %s" % self.race_edition,
                self.get_checkout_url("cancel"),
                self.get_checkout_url("error"),
                self.get_checkout_url("return"),
                self.object.first_name,
                self.object.last_name,
                self.object.email,
                request=self.request,
            )
        except APIResponseError as error:
            logger.error(
                (
                    "Impossible d'initialiser la demande de paiement pour "
                    "l'inscription #%d, erreur de l'API :\n%s"
                ),
                self.object.pk,
                str(error.error_list),
            )
        except HTTPError as error:
            logger.error(
                (
                    "Impossible d'initialiser la demande de paiement pour "
                    "l'inscription #%d, code %d avec comme contenu :\n%s"
                ),
                self.object.pk,
                error.response.status_code,
                str(error.response.content),
            )
        except Exception:
            logger.exception(
                (
                    "Impossible d'initialiser la demande de paiement pour "
                    "l'inscription #%d"
                ),
                self.object.pk,
            )
        else:
            RegistrationPayment.objects.create(
                registration=self.object,
                amount=self.object.total_price,
                payment_means=PaymentMeans.CREDIT_CARD,
                checkout_id=str(checkout_id),
            )
            self.set_session_data("checkout_id", checkout_id)
            return HttpResponseRedirect(redirect_url)

        messages.error(
            self.request,
            (
                "Une erreur est survenue lors de l'initialisation de la "
                "demande de paiement. Veuillez réessayer ou nous contacter "
                "si l'erreur persiste."
            ),
        )
        return HttpResponseRedirect(self.get_register_url("confirmation"))

    def cancel_response(self):
        """
        La demande de paiement est annulée : on supprime l'objet en base avant
        de rediriger vers la dernière étape de l'inscription.
        """
        messages.error(
            self.request,
            "La demande de paiement de votre inscription a été annulée.",
        )
        self.delete_registration_payment()
        return HttpResponseRedirect(self.get_register_url("confirmation"))

    def error_response(self):
        """
        Une erreur technique est survenue : on supprime l'objet en base avant
        de rediriger vers la dernière étape de l'inscription, en prévenant la
        personne de l'incident.
        """
        logger.warning(
            "Une erreur est survenue pour le paiement #%s : %s",
            self.registration_payment.checkout_id,
            self.request.GET.get("error", "-"),
        )
        messages.error(
            self.request,
            (
                "Une erreur est survenue lors du paiement de votre "
                "inscription. Veuillez réessayer ou nous contacter si "
                "l'erreur persiste."
            ),
        )
        self.delete_registration_payment()
        return HttpResponseRedirect(self.get_register_url("confirmation"))

    def refused_response(self):
        """
        Le paiement a été refusé : on supprime l'objet en base avant de
        rediriger vers la dernière étape de l'inscription, en prévenant la
        personne de l'incident.
        """
        logger.warning(
            "Le paiement #%s a été refusé",
            self.registration_payment.checkout_id,
        )
        messages.error(
            self.request,
            (
                "Le paiement de votre inscription a été refusé. Veuillez "
                "réessayer en vérifiant les informations bancaires saisies "
                "ou nous contacter si l'erreur persiste."
            ),
        )
        self.delete_registration_payment()
        return HttpResponseRedirect(self.get_register_url("confirmation"))

    def success_response(self):
        """
        Le paiement a été accepté : on met à jour l'inscription et son paiement
        avant de rediriger vers la fin de l'inscription.
        """
        now = timezone.now()

        self.registration_payment.is_pending = False
        self.registration_payment.created_at = now
        self.registration_payment.save(
            update_fields=["is_pending", "created_at"]
        )

        self.object.status = Registration.Status.NEW
        self.object.created_at = now
        self.object.save(update_fields=["status", "created_at"])

        return HttpResponseRedirect(self.get_register_url("done"))


class RegistrationCancelView(
    RegistrationObjectPageMixin,
    FormMixin,
    TemplateResponseMixin,
    ProcessFormView,
):
    """
    Annule une inscription en cours à une éditon de course.
    """

    form_class = Form
    template_name = "registration/confirm_delete.html"

    #: L'URL de la page d'annulation de l'inscription en cours.
    #: (requis, à définir à l'initialisation)
    cancel_url = None

    #: L'URL de la page de l'inscription en cours.
    #: (requis, à définir à l'initialisation)
    register_url = None

    def get(self, request, *args, **kwargs):
        if self.object is None:
            self.clear_session()
            return HttpResponseRedirect(self.get_success_url())
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        self.object.delete()
        self.clear_session()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["cancel_url"] = self.cancel_url
        context["register_url"] = self.register_url
        return context
