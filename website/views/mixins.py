class PageContextMixin:
    """
    Un mixin qui définis le contexte nécessaire pour le rendu d'une page.
    """

    #: Le titre de la page affiché dans l'en-tête.
    page_title = ""

    #: Le titre de la page pour les méta données (optionnel).
    seo_title = ""

    #: Un résumé du contenu de la page pour les méta données (optionnel).
    search_description = ""

    def get_page_title(self):
        return self.page_title

    def get_seo_title(self):
        return self.seo_title or self.page_title

    def get_search_description(self):
        return self.search_description

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["page_title"] = self.get_page_title()
        context["seo_title"] = self.get_seo_title()
        context["search_description"] = self.get_search_description()
        return context
