from django.apps import AppConfig


class WebsiteConfig(AppConfig):
    name = "website"
    verbose_name = "100km à pied de Steenwerck"
