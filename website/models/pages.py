from django.core.exceptions import ValidationError
from django.db import models

from wagtail.fields import RichTextField, StreamField
from wagtail.models import Page

from modelcluster.fields import ParentalKey

from ..blocks import PARAGRAPH_FEATURES, ContentStreamBlock
from .mixins import BannerMixin, IntroductionMixin, PageContextMixin


class NavigationMenu(models.TextChoices):
    MAIN = "main", "Principal"
    FOOTER = "footer", "Pied de page"


class PageNavigation(models.Model):
    """
    Les pages se trouvant dans le premier niveau d'un menu.
    """

    class Meta:
        unique_together = ["page", "menu"]

    # Champs

    page = ParentalKey(
        "wagtailcore.Page",
        on_delete=models.CASCADE,
        related_name="navigations",
        related_query_name="navigation",
    )

    menu = models.CharField(
        max_length=10,
        choices=NavigationMenu.choices,
        verbose_name="menu",
    )
    order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name="position",
    )


# TYPES DE PAGES DE BASE
# ------------------------------------------------------------------------------


class HomePage(PageContextMixin, Page):
    """
    La page d'accueil du site.
    """

    class Meta:
        verbose_name = "page d'accueil"
        verbose_name_plural = "pages d'accueil"

    # Champs

    is_alert_visible = models.BooleanField(
        default=False,
        verbose_name="afficher l'alerte",
    )
    alert_message = models.TextField(
        blank=True,
        verbose_name="message de l'alerte",
    )
    alert_button_text = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="message de l'alerte",
    )
    alert_button_link = models.ForeignKey(
        "wagtailcore.Page",
        on_delete=models.SET_NULL,
        related_name="+",
        null=True,
        blank=True,
        verbose_name="lien du bouton de l'alerte",
    )

    background_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="image de fond",
    )

    # Configuration de la page

    parent_page_types = ["wagtailcore.Page"]

    template = "pages/home_page.html"

    # Méthodes

    @property
    def has_alert_button(self):
        return self.alert_button_text and self.alert_button_link

    def clean(self):
        if self.is_alert_visible and not self.alert_message:
            raise ValidationError(
                {
                    "alert_message": ValidationError(
                        "Champs requis pour rendre visible le bandeau d'alerte."
                    )
                }
            )


class ContentPage(BannerMixin, IntroductionMixin, PageContextMixin, Page):
    """
    Une page de contenu générique du site.
    """

    class Meta:
        verbose_name = "page de contenu"
        verbose_name_plural = "pages de contenu"

    # Champs

    body = StreamField(
        ContentStreamBlock(),
        verbose_name="contenu de la page",
    )

    # Configuration de la page

    show_in_menus_default = True

    template = "pages/content_page.html"


class LegalsPage(IntroductionMixin, PageContextMixin, Page):
    """
    La page des mentions légales du site.
    """

    class Meta:
        verbose_name = "page des mentions légales"
        verbose_name_plural = "pages des mentions légales"

    # Champs

    body = StreamField(
        ContentStreamBlock(),
        verbose_name="contenu de la page",
    )

    tracking_paragraph = RichTextField(
        features=PARAGRAPH_FEATURES,
        blank=True,
        verbose_name="paragraphe sur l'analyse statistique",
    )

    # Configuration de la page

    max_count_per_parent = 1
    parent_page_types = ["HomePage"]

    show_in_menus_default = True

    template = "pages/legals_page.html"


class IndexPage(IntroductionMixin, PageContextMixin, Page):
    """
    Une page listant automatiquement ses sous-pages.
    """

    class Meta:
        verbose_name = "page d'index"
        verbose_name_plural = "pages d'index"

    # Configuration de la page

    parent_page_types = ["HomePage"]

    show_in_menus_default = True

    template = "pages/index_page.html"

    # Méthodes

    def get_context(self, request):
        context = super().get_context(request)
        context["children"] = self.get_children().live().specific(defer=True)
        return context
