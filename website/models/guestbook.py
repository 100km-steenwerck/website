from django.core.paginator import Paginator
from django.db import models
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.utils.formats import date_format

from wagtail.contrib.routable_page.models import RoutablePageMixin, path
from wagtail.models import Page

from .mixins import BannerMixin, IntroductionMixin, PageContextMixin


class GuestBookEntryQuerySet(models.QuerySet):
    def visible(self):
        return self.filter(is_visible=True)


class GuestBookEntry(models.Model):
    class Meta:
        ordering = ["-date"]
        verbose_name = "message du livre d'or"
        verbose_name_plural = "messages du livre d'or"

    objects = GuestBookEntryQuerySet.as_manager()

    # Champs

    name = models.CharField(max_length=150, verbose_name="nom")
    email = models.EmailField(blank=True, verbose_name="adresse mail")
    message = models.TextField(verbose_name="message")
    date = models.DateTimeField(default=timezone.now, verbose_name="date")

    is_visible = models.BooleanField(default=True, verbose_name="visible")

    # Méthodes

    def __str__(self):
        return "{name} le {date}".format(
            name=self.name,
            date=date_format(self.date),
        )


class GuestBookPage(
    BannerMixin,
    IntroductionMixin,
    PageContextMixin,
    RoutablePageMixin,
    Page,
):
    class Meta:
        verbose_name = "livre d'or"
        verbose_name_plural = "livres d'or"

    # Champs

    per_page = models.PositiveSmallIntegerField(
        default=15,
        verbose_name="messages par page",
    )

    # Configuration de la page

    max_count = 1

    show_in_menus_default = True

    template = "pages/guest_book_page.html"

    # Méthodes

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        if not any(
            getattr(request, key, False)
            for key in ("in_preview_panel", "is_add_entry")
        ):
            # Inclus les messages paginés uniquement si on n'est pas sur la
            # prévisulisation depuis l'éditeur ou dans la vue 'add_entry'
            entries = GuestBookEntry.objects.visible()
            paginator = Paginator(entries, self.per_page)
            page = paginator.get_page(request.GET.get("p", 1))

            context.update(
                {
                    "entries": entries,
                    "page_obj": page,
                }
            )
        return context

    @path("new/")
    def add_entry(self, request):
        from website.forms.guestbook import GuestBookEntryForm

        request.is_add_entry = True

        if request.method == "POST":
            form = GuestBookEntryForm(request.POST)

            if form.is_valid():
                form.save()
                return HttpResponseRedirect(self.get_url(request))
        else:
            form = GuestBookEntryForm()

        return self.render(
            request,
            context_overrides={
                "form": form,
                "seo_title": "Laisser un message dans le livre d'or",
            },
            template="pages/guest_book_page/add_entry.html",
        )
