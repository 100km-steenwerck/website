from django.core.exceptions import PermissionDenied
from django.db import models
from django.utils.functional import cached_property

from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.fields import RichTextField, StreamField

from modelcluster.fields import ParentalKey

from ..blocks import PARAGRAPH_FEATURES, ContentStreamBlock
from .mixins import BannerMixin, IntroductionMixin, PageContextMixin


class VolunteerFormField(AbstractFormField):
    """
    Les champs du formulaire bénévoles.
    """

    page = ParentalKey(
        "VolunteerFormPage",
        on_delete=models.CASCADE,
        related_name="form_fields",
    )


class VolunteerFormPage(
    BannerMixin, IntroductionMixin, PageContextMixin, AbstractEmailForm
):
    """
    La page présentant et traitant le formulaire bénévoles.
    """

    class Meta:
        verbose_name = "formulaire bénévoles"
        verbose_name_plural = "formulaires bénévoles"

    # Champs

    body = StreamField(
        ContentStreamBlock(),
        verbose_name="contenu de la page",
    )

    is_form_active = models.BooleanField(
        default=True,
        verbose_name="formulaire actif",
    )
    success_text = RichTextField(
        features=PARAGRAPH_FEATURES,
        blank=True,
        verbose_name="texte de remerciement",
        help_text=(
            "Ce texte sera affiché une fois le formulaire envoyé, vous "
            "pouvez y remercier la personne mais aussi donner plus de "
            "précisions sur la suite."
        ),
    )

    # Configuration de la page

    max_count_per_parent = 1

    template = "pages/volunteer_form_page.html"
    landing_page_template = "pages/volunteer_form_landing_page.html"

    @cached_property
    def form_builder(self):
        from website.forms.volunteers import VolunteerFormBuilder

        return VolunteerFormBuilder

    def get_form(self, *args, **kwargs):
        if not self.is_form_active:
            raise PermissionDenied(
                "Le formulaire n'est pas actif pour le moment."
            )
        return super().get_form(*args, **kwargs)
