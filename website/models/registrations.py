import logging
from decimal import Decimal
from smtplib import SMTPException

from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import Q, Sum
from django.utils import timezone
from django.utils.functional import cached_property

from wagtail.models import Orderable
from wagtail.search import index

from django_countries.fields import CountryField
from dynamic_filenames import FilePattern
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from phonenumber_field.modelfields import PhoneNumberField

from ..utils import get_age, normalize_email, price_format

logger = logging.getLogger("website.registration")


class RegistrationOption(Orderable):
    """
    Les options d'inscription des éditions de courses que les participant⋅es
    peuvent demander.
    """

    class Meta:
        ordering = ["sort_order"]
        verbose_name = "option d'inscription"
        verbose_name_plural = "options d'inscription"

    # Champs

    race_edition = ParentalKey(
        "RaceEdition",
        on_delete=models.CASCADE,
        related_name="registration_options",
    )

    name = models.CharField(max_length=100, verbose_name="nom")
    description = models.TextField(blank=True, verbose_name="description")
    price = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        verbose_name="prix",
    )

    # Méthodes

    def __str__(self):
        return self.name


class Registration(index.Indexed, ClusterableModel):
    """
    Les inscriptions aux éditions de courses le permettant.
    """

    class Meta:
        verbose_name = "inscription"
        verbose_name_plural = "inscriptions"

    class Sex(models.TextChoices):
        FEMALE = "F", "Femme"
        MALE = "M", "Homme"

    class Status(models.TextChoices):
        DRAFT = "0", "Brouillon"
        NEW = "1", "Nouvelle"
        INCOMPLETE = "2", "Incomplète"
        VALIDATED = "3", "Validée"

    medical_certificate_upload_to_pattern = FilePattern(
        filename_pattern=(
            "registrations/medical_certificates/{instance.race_edition.pk}/"
            "{uuid:base32}{ext}"
        ),
    )

    search_fields = [
        index.AutocompleteField("email"),
        index.SearchField("email"),
        index.AutocompleteField("first_name"),
        index.SearchField("first_name"),
        index.AutocompleteField("last_name"),
        index.SearchField("last_name"),
        index.FilterField("has_email_error"),
        index.FilterField("race_edition"),
        index.FilterField("status"),
        index.FilterField("created_at"),
    ]

    # Champs

    race_edition = models.ForeignKey(
        "RaceEdition",
        on_delete=models.PROTECT,
        related_name="registrations",
        related_query_name="registration",
        limit_choices_to={"has_registration": True},
        verbose_name="course",
    )

    first_name = models.CharField(max_length=150, verbose_name="prénom")
    last_name = models.CharField(max_length=150, verbose_name="nom")
    birthdate = models.DateField(verbose_name="date de naissance")
    sex = models.CharField(
        max_length=1,
        choices=Sex.choices,
        verbose_name="sexe",
    )

    street_address = models.TextField(verbose_name="adresse")
    zip_code = models.CharField(
        max_length=5,
        validators=[RegexValidator(r"\d{3,5}")],
        verbose_name="code postal",
    )
    city = models.CharField(max_length=150, verbose_name="ville")
    country = CountryField(default="FR", verbose_name="pays")

    email = models.EmailField(blank=True, verbose_name="adresse mail")
    phone = PhoneNumberField(verbose_name="téléphone")

    medical_certificate = models.FileField(
        upload_to=medical_certificate_upload_to_pattern,
        null=True,
        blank=True,
        verbose_name="justificatif médical",
    )
    ffa_license_number = models.CharField(
        max_length=30,
        blank=True,
        verbose_name="n° de licence FFA",
    )

    club = models.CharField(max_length=200, blank=True, verbose_name="club")

    comment = models.TextField(blank=True, verbose_name="commentaire")

    options = models.ManyToManyField(
        RegistrationOption,
        related_name="registrations",
        related_query_name="registration",
        through="SubscribedRegistrationOption",
        blank=True,
        verbose_name="options d'inscription",
    )

    race_price = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        verbose_name="tarif de la course",
    )
    total_price = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        editable=False,
        verbose_name="montant total",
    )
    is_payed = models.BooleanField(
        default=False,
        verbose_name="payé",
        help_text=(
            "Indique si le paiement des frais d'inscription est à jour et "
            "terminé. Veuillez vous assurer que le montant payé correspond "
            "bien au montant total de l'inscription."
        ),
    )

    status = models.CharField(
        max_length=1,
        choices=Status.choices,
        default=Status.DRAFT,
        verbose_name="statut",
    )

    has_email_error = models.BooleanField(
        default=False,
        verbose_name="adresse mail en erreur",
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        verbose_name="créée le",
    )

    # Méthodes

    def __str__(self):
        return "{full_name} - {race}".format(
            full_name=self.get_full_name(),
            race=self.race_edition,
        )

    def clean(self):
        if (
            self.birthdate
            and hasattr(self, "race_edition")
            and self.race_edition.registration_required_age
        ):
            age = get_age(self.birthdate, self.race_edition.start_at.date())
            required_age = self.race_edition.registration_required_age

            if age < required_age:
                raise ValidationError(
                    {
                        "birthdate": ValidationError(
                            (
                                "L'âge requis pour participer à la course est "
                                "de %(required_age)d ans."
                            ),
                            code="required_age_not_reached",
                            params={"required_age": required_age},
                        ),
                    }
                )

        # Normalise le prénom, nom et adresse mail
        self.first_name = self.first_name.title()
        self.last_name = self.last_name.upper()
        self.email = normalize_email(self.email)

    def save(self, *args, **kwargs):
        if self.race_price is None:
            # Enregistre le prix actuel de l'édition de la course pour cette
            # inscription, celui-ci pouvant varier au cours du temps et ne doit
            # pas impacter les frais des inscriptions déjà créées
            self.race_price = self.race_edition.registration_price
        if self.total_price is None:
            # Initialise le prix total de la course, sa mise à jour est faite
            # uniquement par la méthode `update_total_price()` appelée depuis
            # les formulaires de création et de modification
            self.total_price = self.race_price
        super().save(*args, **kwargs)

    @cached_property
    def payed_amount(self):
        """Retourne le montant total actuellement payé."""
        return self.payments.aggregate(total=Sum("amount"))["total"] or 0

    @cached_property
    def remaining_amount(self):
        """
        Retourne le montant restant à payer si l'inscription n'est pas marquée
        comme payée.
        """
        return 0 if self.is_payed else self.total_price - self.payed_amount

    def get_full_name(self):
        """Retourne le prénom et le nom de la personne."""
        return "{} {}".format(self.first_name, self.last_name).strip()

    def get_total_price_display(self):
        """Retourne le montant total de l'inscription formaté."""
        return price_format(self.total_price)

    def get_subscribed_options_display(self):
        """Retourne le nom des options souscrites séparés par une virgule."""
        return ", ".join(
            self.subscribed_options.values_list(
                "option__name", flat=True
            ).order_by("option__name")
        )

    def email_person(self, subject, message, from_email=None, **kwargs):
        """
        Envoi un courriel à l'adresse mail attachée à cette inscription, et
        retourne `False` si une erreur est survenue.
        """
        assert self.email, "Aucune adresse mail définie"

        try:
            EmailMessage(
                subject,
                message,
                from_email,
                [self.email],
                **kwargs,
            ).send()
        except SMTPException:
            logger.exception(
                "Impossible d'envoyer le courriel « %s » à l'adresse %s",
                subject,
                self.email,
            )

            # Marque l'adresse mail en erreur
            self.has_email_error = True
            self.save(update_fields=["has_email_error"])

            return False

        return True

    def update_total_price(self):
        """Mets à jour si besoin le montant total de cette inscription."""
        total_price = self.race_price + self.subscribed_options.price()
        if self.total_price != total_price:
            self.total_price = total_price
            self.save(update_fields=["total_price"])


class SubscribedRegistrationOptionQuerySet(models.QuerySet):
    def bulk_create(self, objs, *args, **kwargs):
        for obj in objs:
            if obj.price is None:
                # Enregistre le prix actuel de l'option pour cette inscription,
                # celui-ci pouvant varier au cours du temps et ne doit pas
                # impacter les frais des inscriptions déjà créées
                obj.price = obj.option.price
        return super().bulk_create(objs, *args, **kwargs)

    def price(self):
        """Retourne le montant total des options sélectionnées."""
        return sum(self.values_list("price", flat=True) or [Decimal(0)])


class SubscribedRegistrationOption(models.Model):
    """
    Modèle intermédiaire utilisé pour la relation "many-to-many" du champ
    `Registration.options`. Il permet de définir des informations
    supplémentaires sur l'option souscrite.
    """

    objects = SubscribedRegistrationOptionQuerySet.as_manager()

    # Champs

    option = models.ForeignKey(RegistrationOption, on_delete=models.PROTECT)
    registration = models.ForeignKey(
        Registration,
        on_delete=models.CASCADE,
        related_name="subscribed_options",
        related_query_name="subscribed_option",
    )

    price = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        editable=False,
        verbose_name="prix",
    )

    subscribed_at = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        verbose_name="souscrite le",
    )


class PaymentMeans(models.TextChoices):
    """
    Les moyens de paiement pour l'inscription.
    """

    CASH = "CASH", "Espèces"
    CHEQUE = "CHQ", "Chèque"
    CREDIT_CARD = "CC", "Carte bancaire"


class RegistrationPayment(models.Model):
    """
    Les paiements des inscriptions, de différents moyens.
    """

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["registration", "checkout_id"],
                condition=~Q(checkout_id=""),
                name="unique_registration_checkout_id",
            ),
        ]
        ordering = ["-created_at"]
        verbose_name = "règlement d'inscription"
        verbose_name_plural = "règlements d'inscriptions"

    # Champs

    registration = ParentalKey(
        Registration,
        on_delete=models.CASCADE,
        related_name="payments",
        related_query_name="payment",
    )

    amount = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        verbose_name="montant",
    )
    payment_means = models.CharField(
        max_length=4,
        choices=PaymentMeans.choices,
        verbose_name="moyen de paiement",
    )
    is_pending = models.BooleanField(default=True, verbose_name="en attente")

    checkout_id = models.CharField(
        max_length=100,
        blank=True,
        db_index=True,
        editable=False,
    )

    created_at = models.DateTimeField(
        default=timezone.now,
        verbose_name="créé le",
    )
