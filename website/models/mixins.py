from django.db import models

from wagtail.fields import RichTextField


class BannerMixin(models.Model):
    """
    Un mixin ajoutant la possibilité de définir une bannière pour une page.
    """

    class Meta:
        abstract = True

    # Champs

    banner_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.SET_NULL,
        related_name="+",
        null=True,
        blank=True,
        verbose_name="image de la bannière",
    )


class IntroductionMixin(models.Model):
    """
    Un mixin ajoutant la possibilité de définir un texte d'introduction pour une
    page, pouvant être affiché dans l'en-tête.
    """

    class Meta:
        abstract = True

    # Champs

    introduction = RichTextField(
        features=["bold", "italic"],
        blank=True,
        verbose_name="texte de présentation",
        help_text=(
            "Ce texte peut être affiché dans l'en-tête. Il sera aussi utilisé "
            "pour présenter brièvement cette page si sa page parente est un "
            "index."
        ),
    )

    show_introduction_in_header = models.BooleanField(
        default=False,
        verbose_name="afficher le texte de présentation",
    )


class PageContextMixin:
    """
    Un mixin qui définis le contexte nécessaire pour le rendu d'une page.
    """

    def get_page_title(self):
        return self.title

    def get_seo_title(self):
        return self.seo_title or self.title

    def get_search_description(self):
        return self.search_description

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["page_title"] = self.get_page_title()
        context["seo_title"] = self.get_seo_title()
        context["search_description"] = self.get_search_description()
        return context
