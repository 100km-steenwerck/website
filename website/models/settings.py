from django.db import models

from wagtail.contrib.settings.models import BaseSiteSetting, register_setting


@register_setting
class SiteSettings(BaseSiteSetting):
    """
    Paramètres généraux du site.
    """

    class Meta:
        verbose_name = "paramètres du site"
        verbose_name_plural = "paramètres des sites"

    # Champs

    event_date = models.CharField(
        max_length=100,
        verbose_name="date de l'évènement",
        help_text="Cette date sera affichée dans la barre en haut de page.",
    )

    facebook_url = models.URLField(blank=True, verbose_name="Facebook")

    is_tracking_enabled = models.BooleanField(
        default=False,
        verbose_name="suivre la navigation des visiteurs",
    )
