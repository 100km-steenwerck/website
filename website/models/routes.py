from django.core.validators import FileExtensionValidator
from django.db import models

from wagtail.models import Orderable

from dynamic_filenames import FilePattern
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel

route_loop_gpx_upload_to_pattern = FilePattern(
    filename_pattern="routes/{instance.route.pk}/{name:slug}{ext}"
)


class Route(ClusterableModel):
    class Meta:
        verbose_name = "parcours"
        verbose_name_plural = "parcours"

    # Champs

    name = models.CharField(max_length=150, unique=True, verbose_name="nom")

    # Méthodes

    def __str__(self):
        return self.name


class RouteLoop(Orderable):
    class Meta:
        ordering = ["sort_order"]
        unique_together = ["route", "name"]
        verbose_name = "boucle"
        verbose_name_plural = "boucles"

    class Colors(models.TextChoices):
        RED = "#f44336", "Rouge"
        PINK = "#e91e63", "Rose"
        PURPLE = "#9c27b0", "Violet"
        INDIGO = "#3f51b5", "Indigo"
        BLUE = "#2196f3", "Bleu"
        CYAN = "#00bcd4", "Cyan"
        GREEN = "#4caf50", "Vert"
        YELLOW = "#ffeb3b", "Jaune"
        AMBER = "#ffc107", "Ambre"
        ORANGE = "#ff9800", "Orange"
        BROWN = "#795548", "Marron"
        GRAY = "#9e9e9e", "Gris"

    # Champs

    route = ParentalKey(
        "Route",
        on_delete=models.CASCADE,
        related_name="loops",
        related_query_name="loop",
    )

    name = models.CharField(max_length=50, verbose_name="nom")
    color = models.CharField(
        max_length=7,
        choices=Colors.choices,
        verbose_name="couleur",
    )
    distance = models.DecimalField(
        max_digits=6,
        decimal_places=3,
        verbose_name="distance (en km)",
    )

    gpx_file = models.FileField(
        upload_to=route_loop_gpx_upload_to_pattern,
        validators=[
            FileExtensionValidator(
                allowed_extensions=["gpx"],
                message="Format de fichier non autorisé.",
            ),
        ],
        verbose_name="fichier GPX",
    )
