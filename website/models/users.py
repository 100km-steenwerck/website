from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone

from ..utils import normalize_email


class UserManager(BaseUserManager):
    use_in_migrations = True

    @classmethod
    def normalize_email(cls, email):
        return normalize_email(email)

    def _create_user(self, email, password, **extra_fields):
        """
        Crée et enregistre un compte utilisateur avec l'adresse mail et le
        mot de passe donnés.
        """
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_superuser", True)
        if extra_fields.get("is_superuser") is not True:  # pragma: no cover
            raise ValueError("Superuser must have is_superuser=True.")
        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    Le compte utilisateur d'une personne.

    L'authentification se fait avec l'adresse mail, il n'y a pas de nom
    d'utilisateur.
    """

    class Meta:
        ordering = ["first_name", "last_name"]
        verbose_name = "compte utilisateur"
        verbose_name_plural = "comptes utilisateur"

    # Champs

    email = models.EmailField(
        "adresse mail",
        unique=True,
        error_messages={
            "unique": "Un compte avec cette adresse mail existe déjà."
        },
    )
    first_name = models.CharField("prénom", max_length=150, blank=True)
    last_name = models.CharField("nom", max_length=150, blank=True)

    is_active = models.BooleanField(
        "actif",
        default=True,
        help_text=(
            "Détermine si ce compte doit être considéré comme actif au "
            "sein du site (pour se connecter, recevoir les notifications…). "
            "Décochez ceci plutôt que de le supprimer afin de garder son "
            "historique par exemple."
        ),
    )

    date_created = models.DateTimeField(
        "date de création", default=timezone.now, editable=False
    )

    # Configuration du modèle

    objects = UserManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    # Méthodes

    @property
    def is_staff(self):
        """Détermine si le compte a accès à l'administration."""
        return self.has_perms(["wagtailadmin.access_admin"])

    def __str__(self):
        return self.get_full_name()

    def clean(self):
        super().clean()

        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """Retourne le prénom et le nom."""
        return "{} {}".format(self.first_name, self.last_name).strip()

    def get_short_name(self):
        """Retourne le prénom."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Envoi un courriel à cette personne."""
        send_mail(subject, message, from_email, [self.email], **kwargs)
