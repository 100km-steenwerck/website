from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.utils import timezone
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from wagtail.contrib.routable_page.models import RoutablePageMixin, path
from wagtail.fields import StreamField
from wagtail.models import Page

from modelcluster.models import ClusterableModel

from ..blocks import ContentStreamBlock
from .mixins import BannerMixin, IntroductionMixin, PageContextMixin


class RaceEditionQuerySet(models.QuerySet):
    def currents(self):
        return self.filter(is_current=True)

    def current(self):
        try:
            return self.get(is_current=True)
        except ObjectDoesNotExist:
            return None


class RaceEdition(ClusterableModel):
    """
    Les éditions des courses avec leurs paramètres d'inscription.
    """

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["race"],
                condition=Q(is_current=True),
                name="unique_current_race_edition",
            ),
            models.UniqueConstraint(
                fields=["race", "issue"],
                name="unique_race_edition_issue",
            ),
        ]
        ordering = ["-issue", "start_at"]
        verbose_name = "édition de course"
        verbose_name_plural = "éditions de courses"

    objects = RaceEditionQuerySet.as_manager()

    # Champs

    race = models.ForeignKey(
        "RacePage",
        on_delete=models.PROTECT,
        related_name="editions",
        related_query_name="edition",
        verbose_name="course",
    )

    issue = models.PositiveSmallIntegerField(
        db_index=True,
        verbose_name="numéro de l'édition",
    )

    distance = models.DecimalField(
        max_digits=6,
        decimal_places=3,
        verbose_name="distance (en km)",
    )
    start_at = models.DateTimeField(verbose_name="date et heure de départ")

    is_current = models.BooleanField(
        default=False,
        verbose_name="édition en cours",
    )

    race_rules = models.ForeignKey(
        "wagtaildocs.Document",
        on_delete=models.PROTECT,
        related_name="+",
        blank=True,
        null=True,
        verbose_name="règlement de la course",
    )
    sale_conditions = models.ForeignKey(
        "wagtaildocs.Document",
        on_delete=models.PROTECT,
        related_name="+",
        blank=True,
        null=True,
        verbose_name="conditions de vente",
    )

    has_registration = models.BooleanField(
        default=False,
        verbose_name="inscription possible",
    )
    registration_price = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name="prix de l'inscription",
    )
    registration_opening_at = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name="date d'ouverture des inscriptions",
    )
    registration_ending_at = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name="date de fermeture des inscriptions",
    )
    registration_required_age = models.PositiveSmallIntegerField(
        blank=True,
        null=True,
        verbose_name="âge requis pour s'inscrire",
    )

    REQUIRED_REGISTRATION_FIELDS = (
        "race_rules",
        "sale_conditions",
        "registration_price",
        "registration_opening_at",
        "registration_ending_at",
    )

    # Méthodes

    def __str__(self):
        return "{} - {}".format(self.race.title, self.get_issue_display())

    def get_issue_display(self):
        return "{}e édition".format(self.issue)

    def get_issue_html_display(self):
        return format_html("{}<sup>e</sup> édition", self.issue)

    def can_register(self):
        """Détermine s'il est possible de s'inscrire à cette édition."""
        now = timezone.now()
        return (
            self.has_registration
            and now >= self.registration_opening_at
            and now < self.registration_ending_at
        )

    def is_registration_ended(self):
        """Détermine si les inscriptions à cette édition sont closes."""
        return (
            self.has_registration
            and timezone.now() >= self.registration_ending_at
        )

    def clean(self):
        if self.has_registration:
            self.clean_required_registration_fields()

            if self.registration_ending_at <= self.registration_opening_at:
                raise ValidationError(
                    {
                        "registration_ending_at": ValidationError(
                            "Cette date doit être après celle d'ouverture.",
                            code="registration_ending_before_opening",
                        ),
                    }
                )

    def clean_required_registration_fields(self):
        errors = {}

        for field_name in self.REQUIRED_REGISTRATION_FIELDS:
            if not getattr(self, field_name):
                errors[field_name] = ValidationError(
                    "Champs requis.",
                    code="required",
                )

        if errors:
            raise ValidationError(errors)


# PAGES
# ------------------------------------------------------------------------------


class RacePage(
    BannerMixin,
    IntroductionMixin,
    PageContextMixin,
    RoutablePageMixin,
    Page,
):
    """
    Les pages de présentation des courses, actuelles et anciennes.
    """

    class Meta:
        verbose_name = "course"
        verbose_name_plural = "courses"

    # Champs

    body = StreamField(
        ContentStreamBlock(),
        verbose_name="contenu de la page",
    )

    # Configuration de la page

    parent_page_types = ["RaceIndexPage"]
    subpage_types = []

    show_in_menus_default = True

    template = "pages/race_page.html"

    # Méthodes

    def get_context(self, request):
        context = super().get_context(request)
        context["current_edition"] = self.editions.current()
        return context

    def get_edition_for_registration(self, edition_issue):
        """Récupère l'édition donnée de cette course pour l'inscription."""
        try:
            return self.editions.get(
                issue=edition_issue,
                has_registration=True,
            )
        except RaceEdition.DoesNotExist:
            raise Http404(
                "Il n'est soit pas possible de s'inscrire à l'édition de "
                "la course demandée, ou alors celle-ci n'existe pas."
            )

    def get_register_url(self, edition_issue, request=None, **kwargs):
        return self.get_url(request=request) + self.reverse_subpage(
            "register",
            kwargs={"edition_issue": edition_issue, **kwargs},
        )

    def get_registration_cancel_url(self, edition_issue, request=None):
        return self.get_url(request=request) + self.reverse_subpage(
            "registration_cancel",
            kwargs={"edition_issue": edition_issue},
        )

    def get_registration_checkout_url(
        self, edition_issue, request=None, **kwargs
    ):
        return self.get_url(request=request) + self.reverse_subpage(
            "registration_checkout",
            kwargs={"edition_issue": edition_issue, **kwargs},
        )

    # Vues additionnelles

    @path("edition/<int:edition_issue>/register/")
    @path("edition/<int:edition_issue>/register/<slug:step>/")
    def register(self, request, edition_issue, step=None):
        from website.views.registration import RegisterView

        race_edition = self.get_edition_for_registration(edition_issue)

        if not race_edition.can_register():
            if race_edition.is_registration_ended():
                msg = "Les inscriptions pour la %s sont terminées."
            else:
                msg = "Les inscriptions pour la %s ne sont pas encore ouvertes."
            messages.error(
                request,
                mark_safe(msg % race_edition.get_issue_html_display()),
            )
            return HttpResponseRedirect(self.url)

        def get_step_url(step):
            return self.get_register_url(
                edition_issue, step=step, request=request
            )

        view = RegisterView.as_view(
            race_edition=race_edition,
            step_url_kwarg="step",
            get_step_url=get_step_url,
            cancel_url=self.get_registration_cancel_url(
                edition_issue, request=request
            ),
            checkout_url=self.get_registration_checkout_url(
                edition_issue, request=request
            ),
        )
        return view(request, step=step)

    @path("edition/<int:edition_issue>/registration/cancel/")
    def registration_cancel(self, request, edition_issue):
        from website.views.registration import RegistrationCancelView

        race_edition = self.get_edition_for_registration(edition_issue)

        view = RegistrationCancelView.as_view(
            race_edition=race_edition,
            cancel_url=self.get_registration_cancel_url(
                edition_issue, request=request
            ),
            register_url=self.get_register_url(edition_issue, request=request),
            success_url=self.url,
        )
        return view(request)

    @path("edition/<int:edition_issue>/registration/checkout/")
    @path("edition/<int:edition_issue>/registration/checkout/<slug:callback>/")
    def registration_checkout(self, request, edition_issue, callback=None):
        from website.views.registration import RegistrationCheckOutView

        race_edition = self.get_edition_for_registration(edition_issue)

        def get_checkout_url(callback):
            return self.get_registration_checkout_url(
                edition_issue, callback=callback, request=request
            )

        def get_register_url(step=None):
            if step:
                return self.get_register_url(
                    edition_issue, step=step, request=request
                )
            return self.get_register_url(edition_issue, request=request)

        view = RegistrationCheckOutView.as_view(
            race_edition=race_edition,
            callback_url_kwarg="callback",
            get_checkout_url=get_checkout_url,
            get_register_url=get_register_url,
        )
        return view(request, callback=callback)


class RaceIndexPage(PageContextMixin, Page):
    """
    La page listant les courses existantes, définies comme sous-page.
    """

    class Meta:
        verbose_name = "page d'index de courses"
        verbose_name_plural = "pages d'index de courses"

    # Configuration de la page

    max_count_per_parent = 1

    parent_page_types = ["HomePage"]
    subpage_types = ["RacePage"]

    show_in_menus_default = True

    template = "pages/race_index_page.html"

    # Méthodes

    def get_context(self, request):
        context = super().get_context(request)
        context["children"] = RacePage.objects.child_of(self).live()
        return context
