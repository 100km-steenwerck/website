from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = "website.admin"
    verbose_name = "Admin"

    def ready(self):
        from .panels import set_models_panels

        set_models_panels()
