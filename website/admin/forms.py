from django import forms

from wagtail.admin.forms import WagtailAdminPageForm

from website.models import RaceEdition, Registration


class RaceEditionForm(WagtailAdminPageForm):
    """
    Formulaire de création et de modification de l'édition d'une course.
    """

    class Media:
        js = ("admin/js/race-edition-form.js",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # La sélection de la course n'est possible qu'à la création
        if self.instance.pk is not None:
            del self.fields["race"]


class RacePageForm(WagtailAdminPageForm):
    """
    Formulaire de création et de modification d'une page « Course ».
    """

    current_edition = forms.ModelChoiceField(
        queryset=RaceEdition.objects.none(),
        empty_label="Aucune",
        label="Édition en cours",
        required=False,
    )

    def __init__(self, *args, instance=None, **kwargs):
        initial = kwargs.pop("initial", {})

        if instance and instance.pk is not None:
            if current_edition := instance.editions.current():
                initial["current_edition"] = current_edition.pk

        super().__init__(*args, initial=initial, instance=instance, **kwargs)

        if self.instance.pk is not None:
            self.fields["current_edition"].queryset = self.instance.editions

    def save(self, commit=True):
        editions_qs = self.instance.editions
        # Mets à jour les éditions de la course en fonction de celle en cours
        # sans tenir de compte de la valeur de `commit` à ce stade
        if current_edition := self.cleaned_data["current_edition"]:
            editions_qs.currents().exclude(pk=current_edition.pk).update(
                is_current=False
            )
            editions_qs.filter(pk=current_edition.pk, is_current=False).update(
                is_current=True
            )
        else:
            editions_qs.currents().update(is_current=False)
        return super().save(commit=commit)


class RegistrationForm(WagtailAdminPageForm):
    """
    Formulaire de création et de modification d'une inscription.
    """

    status = forms.ChoiceField(
        choices=(
            (Registration.Status.INCOMPLETE, "Incomplète"),
            (Registration.Status.VALIDATED, "Valide"),
        ),
        initial=Registration.Status.INCOMPLETE,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance.pk is None:
            del self.fields["status"]
        else:
            # Filtre les options d'inscription pour ne présenter que celles
            # définies par l'édition de la course sélectionnée
            options = self.fields["options"]
            options.queryset = options.queryset.filter(
                race_edition=self.instance.race_edition_id
            )

    def _save_m2m(self):
        super()._save_m2m()

        # Mets à jour le montant total de l'inscription lorsque les relations
        # "many-to-many" (utilisées par les options) sont enregistrées
        self.instance.update_total_price()
