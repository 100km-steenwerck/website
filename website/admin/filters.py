from django.core.validators import EMPTY_VALUES

from wagtail.admin.filters import WagtailFilterSet
from wagtail.admin.widgets import BooleanRadioSelect

import django_filters

from website.models import RaceEdition, Registration
from website.widgets import NativeDateInput

VISIBLE_CHOICES = (("true", "Oui"), ("false", "Non"))


class EmptyStringFilter(django_filters.BooleanFilter):
    """
    Filtre sur une chaine de caractère vide ou non.
    """

    def filter(self, queryset, value):
        if value in EMPTY_VALUES:
            return queryset

        exclude = self.exclude ^ (value is False)
        method = queryset.exclude if exclude else queryset.filter

        return method(**{self.field_name: ""})


class RegistrationFilterSet(WagtailFilterSet):
    class Meta:
        model = Registration
        fields = [
            "race_edition",
            "status",
            "has_email_error",
            "has_pending_paiement",
        ]

    # Champs

    race_edition = django_filters.ModelChoiceFilter(
        queryset=RaceEdition.objects.filter(has_registration=True),
    )
    status = django_filters.MultipleChoiceFilter(
        choices=Registration.Status.choices,
    )
    has_pending_paiement = django_filters.BooleanFilter(
        field_name="payment__is_pending",
        label="Paiement en attente",
        widget=BooleanRadioSelect,
    )
    created_before = django_filters.DateFilter(
        field_name="created_at",
        lookup_expr="lt",
        label="Inscription avant le",
        widget=NativeDateInput,
    )
    created_after = django_filters.DateFilter(
        field_name="created_at",
        lookup_expr="gt",
        label="Inscription après le",
        widget=NativeDateInput,
    )


class RegistrationsReportFilterSet(WagtailFilterSet):
    class Meta:
        model = Registration
        fields = [
            "race_edition",
            "status",
            "has_medical_certificate",
            "is_payed",
        ]

    # Champs

    race_edition = django_filters.ModelChoiceFilter(
        queryset=RaceEdition.objects.filter(has_registration=True),
    )
    has_medical_certificate = EmptyStringFilter(
        field_name="medical_certificate",
        exclude=True,
        label="Justificatif médical",
        widget=BooleanRadioSelect,
    )
    created_before = django_filters.DateFilter(
        field_name="created_at",
        lookup_expr="date__lt",
        label="Inscription avant le",
        widget=NativeDateInput,
    )
    created_after = django_filters.DateFilter(
        field_name="created_at",
        lookup_expr="date__gt",
        label="Inscription après le",
        widget=NativeDateInput,
    )
