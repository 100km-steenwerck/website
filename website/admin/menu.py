from wagtail.admin.menu import MenuItem


class PermissionCheckedMenuItem(MenuItem):
    def __init__(
        self,
        label,
        url,
        permission_policy,
        any_permission_required=["view", "add", "change", "delete"],
        **kwargs,
    ):
        super().__init__(label, url, **kwargs)
        self.permission_policy = permission_policy
        self.any_permission_required = any_permission_required

    def is_shown(self, request):
        return self.permission_policy.user_has_any_permission(
            request.user, self.any_permission_required
        )
