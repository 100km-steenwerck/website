from django.urls import path, reverse

from wagtail import hooks
from wagtail.admin.menu import MenuItem
from wagtail.admin.ui.sidebar import LinkMenuItem as LinkMenuItemComponent
from wagtail.admin.viewsets.base import ViewSetGroup
from wagtail.models import Site

from .menu import PermissionCheckedMenuItem
from .views.guestbook import GuestBookEntryViewSet
from .views.race_editions import RaceEditionViewSet
from .views.registrations import RegistrationViewSet
from .views.registrations import bulk_actions as registrations_bulk_actions
from .views.reports import RegistrationsReportView
from .views.routes import RouteViewSet

raceeditions_viewset = RaceEditionViewSet(
    "admin_raceeditions",
    menu_label="Éditions",
    url_prefix="races/editions",
)
registrations_viewset = RegistrationViewSet(
    "admin_registrations",
    menu_label="Inscriptions",
    url_prefix="races/registrations",
)
routes_viewset = RouteViewSet(
    "admin_routes",
    menu_label="Parcours",
    url_prefix="routes",
)


class RaceViewSetGroup(ViewSetGroup):
    menu_label = "Courses"
    menu_icon = "person-walking"
    menu_order = 7000
    items = (raceeditions_viewset, registrations_viewset, routes_viewset)


class RootPageMenuItem(MenuItem):
    def __init__(
        self,
        label="Retour au site",
        name="root-page",
        icon_name="home",
        order=20000,
    ):
        super().__init__(
            label,
            url="",
            name=name,
            icon_name=icon_name,
            order=order,
        )

    def is_active(self, request):
        return False

    def render_component(self, request):
        return LinkMenuItemComponent(
            self.name,
            self.label,
            self.get_root_page_url(request),
            icon_name=self.icon_name,
        )

    def get_root_page_url(self, request):
        site = Site.find_for_request(request)
        root_url = site.root_page.get_url(request, site)
        return request.build_absolute_uri(root_url)


@hooks.register("register_admin_menu_item")
def register_root_page_menu_item():
    return RootPageMenuItem()


@hooks.register("register_admin_viewset")
def register_race_viewsetgroup():
    return RaceViewSetGroup()


@hooks.register("register_admin_viewset")
def register_guestbookentry_viewset():
    return GuestBookEntryViewSet(
        "admin_guestbook",
        add_to_admin_menu=True,
        menu_label="Livre d'or",
        menu_order=8000,
        url_prefix="guestbook",
    )


@hooks.register("register_reports_menu_item")
def register_unpublished_changes_report_menu_item():
    return PermissionCheckedMenuItem(
        "Inscriptions",
        reverse("admin_registrations_report"),
        RegistrationsReportView.permission_policy,
        icon_name=RegistrationsReportView.header_icon,
        order=700,
    )


@hooks.register("register_admin_urls")
def register_reports_url():
    return [
        path(
            "reports/registrations/",
            RegistrationsReportView.as_view(),
            name=RegistrationsReportView.index_url_name,
        ),
        path(
            "reports/registrations/results/",
            RegistrationsReportView.as_view(results_only=True),
            name=RegistrationsReportView.index_results_url_name,
        ),
    ]


@hooks.register("register_icons")
def register_icons(icons):
    for icon in ["book", "clipboard-user", "images", "person-walking", "route"]:
        icons.append("admin/icons/{}.svg".format(icon))
    return icons


for action_class in [registrations_bulk_actions.DeleteBulkAction]:
    hooks.register("register_bulk_action", action_class)
