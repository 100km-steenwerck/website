from django import forms

from wagtail.admin.panels import (
    FieldPanel,
    FieldRowPanel,
    HelpPanel,
    InlinePanel,
    MultiFieldPanel,
    ObjectList,
    PageChooserPanel,
    Panel,
    TabbedInterface,
    TitleFieldPanel,
)
from wagtail.admin.widgets.slug import SlugInput
from wagtail.coreutils import multigetattr

from website.utils import price_format
from website.widgets import NativeDateInput, NativeDateTimeInput


class ValuePanel(Panel):
    """
    Affiche la valeur d'un champ ou celle retournée par une méthode définie
    par le modèle de l'objet.
    """

    def __init__(self, accessor, **kwargs):
        super().__init__(**kwargs)
        self.accessor = accessor

    def clone_kwargs(self):
        kwargs = super().clone_kwargs()
        kwargs["accessor"] = self.accessor
        return kwargs

    class BoundPanel(Panel.BoundPanel):
        template_name = "admin/panels/value_panel.html"

        def get_context_data(self, parent_context=None):
            context = super().get_context_data(parent_context)
            context["display_value"] = self.get_display_value()
            return context

        def get_display_value(self):
            if callable(self.panel.accessor):
                return self.panel.accessor(self.instance)
            else:
                return multigetattr(self.instance, self.panel.accessor) or ""


def set_pages_panels():
    from wagtail.models import Page

    from website.models import (
        ContentPage,
        GuestBookPage,
        HomePage,
        IndexPage,
        LegalsPage,
        RaceIndexPage,
        RacePage,
        VolunteerFormPage,
    )

    from .forms import RacePageForm

    Page.content_panels = [
        TitleFieldPanel(
            "title",
            classname="title",
            help_text="Le nom de la page à présenter dans le site.",
        ),
    ]

    Page.promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("slug", widget=SlugInput),
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
            ],
            "Pour les moteurs de recherche",
        ),
        MultiFieldPanel(
            [
                FieldPanel(
                    "show_in_menus",
                    help_text=(
                        "Si cette page est à inclure dans les menus, soit "
                        "directement en choisissant le menu ci-dessous, soit "
                        "au sein d'un sous-menu de sa page parente."
                    ),
                ),
                InlinePanel(
                    "navigations",
                    panels=[
                        FieldRowPanel(
                            [
                                FieldPanel("menu", classname="col7"),
                                FieldPanel("order", classname="col5"),
                            ]
                        ),
                    ],
                    label="Menu de navigation",
                    heading="Menus de navigation",
                    help_text=(
                        "Choisissez le ou les menus du site dans lequel cette "
                        "page est à inclure dans son premier niveau."
                    ),
                ),
            ],
            "Pour les menus du site",
        ),
    ]

    # PAGES DE BASE
    # --------------------------------------------------------------------------

    HomePage.content_panels = Page.content_panels + [
        FieldPanel("background_image"),
        MultiFieldPanel(
            [
                FieldPanel("is_alert_visible", heading="Visible"),
                FieldPanel("alert_message", heading="Message"),
                FieldRowPanel(
                    [
                        FieldPanel(
                            "alert_button_text", heading="Texte du bouton"
                        ),
                        PageChooserPanel(
                            "alert_button_link", heading="Lien du bouton"
                        ),
                    ],
                    help_text=(
                        "Pour afficher un bouton à la suite du message, "
                        "veuillez saisir son texte et son lien."
                    ),
                ),
            ],
            heading="Bandeau d'alerte",
        ),
    ]

    ContentPage.content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("banner_image"),
                FieldPanel("introduction"),
                FieldPanel("show_introduction_in_header"),
            ],
            heading="En-tête",
        ),
        FieldPanel("body"),
    ]

    LegalsPage.content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("introduction"),
                FieldPanel("show_introduction_in_header"),
            ],
            heading="En-tête",
        ),
        FieldPanel("tracking_paragraph"),
        FieldPanel("body"),
    ]

    IndexPage.content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("introduction"),
                FieldPanel("show_introduction_in_header"),
            ],
            heading="En-tête",
        ),
    ]

    # LIVRE D'OR
    # --------------------------------------------------------------------------

    GuestBookPage.content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("banner_image"),
                FieldPanel("introduction"),
                FieldPanel("show_introduction_in_header"),
            ],
            heading="En-tête",
        ),
        MultiFieldPanel(
            [
                FieldPanel("per_page"),
            ],
            heading="Configuration",
        ),
    ]

    # COURSES
    # --------------------------------------------------------------------------

    RacePage.content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("banner_image"),
                FieldPanel("introduction"),
                FieldPanel("show_introduction_in_header"),
            ],
            heading="En-tête",
        ),
        FieldPanel("current_edition"),
        FieldPanel("body"),
    ]
    RacePage.base_form_class = RacePageForm

    RaceIndexPage.content_panels = Page.content_panels

    # BENEVOLES
    # --------------------------------------------------------------------------

    VolunteerFormPage.content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("banner_image"),
                FieldPanel("introduction"),
                FieldPanel("show_introduction_in_header"),
            ],
            heading="En-tête",
        ),
        FieldPanel("body"),
    ]
    VolunteerFormPage.form_panels = [
        FieldPanel("is_form_active", heading="Actif"),
        FieldPanel("success_text"),
        MultiFieldPanel(
            [
                FieldPanel(
                    "to_address",
                    heading="Adresse(s) destinataire(s)",
                    help_text=(
                        "Un courriel sera envoyé à chaque envoi du formulaire "
                        "à cette adresses mail. Il est possible d'en définir "
                        "plusieurs en les séparant par des virgules."
                    ),
                ),
                FieldPanel("subject", heading="Sujet du courriel"),
            ],
            heading="Envoi des données par mail",
        ),
        InlinePanel(
            "form_fields",
            label="Champ",
            heading="Champs du formulaire",
        ),
    ]
    VolunteerFormPage.edit_handler = TabbedInterface(
        [
            ObjectList(VolunteerFormPage.content_panels, heading="Contenu"),
            ObjectList(VolunteerFormPage.form_panels, heading="Formulaire"),
            ObjectList(VolunteerFormPage.promote_panels, heading="Promotion"),
            ObjectList(VolunteerFormPage.settings_panels, heading="Paramètres"),
        ],
    )


def set_models_panels():
    from website.models import (
        RaceEdition,
        Registration,
        Route,
        SiteSettings,
    )

    from .forms import RaceEditionForm, RegistrationForm

    set_pages_panels()

    # PARAMETRES
    # --------------------------------------------------------------------------

    SiteSettings.panels = [
        FieldPanel("event_date"),
        MultiFieldPanel(
            [
                FieldPanel("facebook_url"),
            ],
            heading="Liens vers les réseaux",
        ),
        FieldPanel("is_tracking_enabled"),
    ]

    # PARCOURS
    # --------------------------------------------------------------------------

    Route.panels = [
        FieldPanel("name"),
        InlinePanel(
            "loops",
            panels=[
                FieldPanel("name"),
                FieldRowPanel(
                    [
                        FieldPanel("color"),
                        FieldPanel("distance"),
                    ]
                ),
                FieldPanel(
                    "gpx_file",
                    widget=forms.ClearableFileInput(
                        attrs={"accept": ".gpx,application/gpx+xml"},
                    ),
                ),
            ],
            min_num=1,
            label="Boucle",
            heading="Boucles",
        ),
    ]

    # EDITIONS
    # --------------------------------------------------------------------------

    RaceEdition.panels = [
        FieldPanel("race", widget=forms.Select),
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel("issue"),
                        FieldPanel("start_at", widget=NativeDateTimeInput),
                    ],
                ),
                FieldPanel("distance"),
            ],
            heading="Détails",
        ),
        FieldPanel("has_registration", heading="Activer l'inscription"),
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel(
                            "registration_opening_at",
                            heading="Date d'ouverture",
                            widget=NativeDateTimeInput,
                        ),
                        FieldPanel(
                            "registration_ending_at",
                            heading="Date de fermeture",
                            widget=NativeDateTimeInput,
                        ),
                    ]
                ),
                FieldRowPanel(
                    [
                        FieldPanel(
                            "registration_price",
                            heading="Prix",
                        ),
                        FieldPanel(
                            "registration_required_age",
                            heading="Âge requis",
                        ),
                    ],
                ),
                FieldPanel("race_rules"),
                FieldPanel("sale_conditions"),
                InlinePanel(
                    "registration_options",
                    panels=[
                        FieldRowPanel(
                            [
                                FieldPanel("name", classname="col8"),
                                FieldPanel("price", classname="col4"),
                            ]
                        ),
                        FieldPanel("description"),
                    ],
                    label="Option",
                    heading="Options",
                ),
            ],
            heading="Paramètres d'inscription",
            attrs={"data-registration-settings": ""},
        ),
    ]
    RaceEdition.base_form_class = RaceEditionForm

    # INSCRIPTIONS
    # --------------------------------------------------------------------------

    Registration.identity_panels = [
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel("first_name"),
                        FieldPanel("last_name"),
                    ],
                ),
                FieldPanel("sex"),
                FieldPanel("birthdate", widget=NativeDateInput),
            ],
            heading="Identité",
        ),
        MultiFieldPanel(
            [
                FieldPanel("street_address"),
                FieldRowPanel(
                    [
                        FieldPanel("zip_code", classname="col4"),
                        FieldPanel("city", classname="col8"),
                    ],
                ),
                FieldPanel("country"),
            ],
            heading="Adresse",
        ),
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel("email", classname="col10"),
                        FieldPanel(
                            "has_email_error",
                            classname="col2",
                            heading="En erreur",
                        ),
                    ],
                ),
                FieldPanel("phone"),
            ],
            heading="Contacts",
        ),
        MultiFieldPanel(
            [
                FieldPanel(
                    "medical_certificate",
                    widget=forms.ClearableFileInput(
                        attrs={"accept": "application/pdf,image/*"},
                    ),
                ),
                FieldPanel("ffa_license_number"),
                FieldPanel("club"),
            ],
            heading="Justificatif médical et licence",
        ),
        FieldPanel("comment"),
    ]
    Registration.race_panels = [
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel("race_edition", classname="col7"),
                        FieldPanel("race_price", classname="col5"),
                    ],
                ),
            ],
            heading="Course",
        ),
        FieldPanel("options", widget=forms.CheckboxSelectMultiple),
        InlinePanel(
            "payments",
            panels=[
                FieldRowPanel(
                    [
                        FieldPanel("created_at", heading="Date"),
                        FieldPanel("payment_means"),
                        FieldPanel("amount"),
                    ]
                ),
                FieldPanel(
                    "is_pending",
                    help_text=(
                        "Indique que ce paiement n'a pas été reçu, peut-être à "
                        "cause d'un problème."
                    ),
                ),
            ],
            label="Paiement",
            heading="Paiements",
        ),
        MultiFieldPanel(
            [
                HelpPanel(
                    content=(
                        """
                        <div class="help-block help-info">
                          <svg class="icon icon-help" aria-hidden="true">
                            <use href="#icon-help"></use>
                          </svg>
                          <p>
                            En cas de changement des options ou du tarif la
                            course, veuillez enregistrer pour que le montant
                            total soit recalculé.
                          </p>
                        </div>
                        """
                    ),
                ),
                FieldRowPanel(
                    [
                        ValuePanel(
                            lambda o: price_format(o.payed_amount),
                            heading="Montant payé",
                        ),
                        ValuePanel(
                            "get_total_price_display",
                            heading="Montant total",
                        ),
                    ]
                ),
                FieldPanel("is_payed", heading="Marquer comme payée"),
            ],
            heading="État du paiement",
        ),
        FieldPanel("status"),
        ValuePanel("created_at", heading="Date d'inscription"),
    ]
    Registration.edit_handler = TabbedInterface(
        [
            ObjectList(
                Registration.identity_panels,
                heading="Informations personnelles",
            ),
            ObjectList(
                Registration.race_panels,
                heading="Course et paiement",
            ),
        ]
    )
    Registration.base_form_class = RegistrationForm
