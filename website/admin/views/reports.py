from wagtail.admin.ui.tables import Column, DateColumn
from wagtail.admin.views.reports import ReportView

from website.admin.filters import RegistrationsReportFilterSet
from website.admin.permissions import registration_permission_policy
from website.models import Registration


class RegistrationsReportView(ReportView):
    page_title = "Inscriptions"
    header_icon = "clipboard-user"
    index_url_name = "admin_registrations_report"
    index_results_url_name = "admin_registrations_report_results"
    filterset_class = RegistrationsReportFilterSet
    permission_policy = registration_permission_policy
    any_permission_required = ["view", "add", "change", "delete"]
    default_ordering = ["last_name"]

    columns = [
        Column("last_name", label="Nom", sort_key="last_name"),
        Column("first_name", label="Prénom", sort_key="first_name"),
        Column("email", label="Adresse mail"),
        Column("phone", label="Téléphone"),
        Column("race_edition", label="Course"),
        DateColumn(
            "created_at",
            label="Date d'inscription",
            sort_key="created_at",
        ),
    ]

    list_export = [
        "last_name",
        "first_name",
        "sex",
        "birthdate",
        "street_address",
        "zip_code",
        "city",
        "country",
        "email",
        "phone",
        "medical_certificate",
        "ffa_license_number",
        "club",
        "comment",
        "remaining_amount",
        "get_subscribed_options_display",
    ]
    export_headings = {
        "remaining_amount": "Restant à payer",
        "get_subscribed_options_display": "Options",
    }

    def get_queryset(self):
        return Registration.objects.select_related("race_edition__race")

    def preprocess_field_value(self, field, value, export_format):
        if field == "medical_certificate":
            return "Oui" if value else "Non"
        return super().preprocess_field_value(field, value, export_format)
