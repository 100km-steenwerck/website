from django.utils.functional import cached_property

from wagtail.admin.ui.tables import BooleanColumn, Column, DateColumn
from wagtail.admin.views import generic
from wagtail.admin.viewsets.model import ModelViewSet

from website.models import GuestBookEntry


class MessageColumn(Column):
    cell_template_name = "admin/tables/guestbook_message_cell.html"


# VIEWS
# ------------------------------------------------------------------------------


class GuestBookEntryIndexView(generic.IndexView):
    add_item_label = "Ajouter un message"
    list_filter = ["is_visible"]

    @cached_property
    def columns(self):
        return [
            DateColumn("date", label="Date", sort_key="date", width=200),
            self._get_title_column("name", width=300),
            MessageColumn("message", label="Message"),
            BooleanColumn("is_visible", label="Visible"),
        ]


class GuestBookEntryCreateView(generic.CreateView):
    success_message = "Message « %(object)s » ajouté."
    error_message = "Le message ne peut être créé du fait d'erreurs."

    def get_breadcrumbs_items(self):
        items = super().get_breadcrumbs_items()
        items[-1]["label"] = "Nouveau message"
        return items


class GuestBookEntryEditView(generic.EditView):
    success_message = "Message « %(object)s » mis à jour."
    error_message = "Le message ne peut être enregistré du fait d'erreurs."


class GuestBookEntryDeleteView(generic.DeleteView):
    confirmation_message = "Êtes-vous sûr⋅e de vouloir supprimer ce message ?"
    success_message = "Message « %(object)s » supprimé."


class GuestBookEntryViewSet(ModelViewSet):
    model = GuestBookEntry
    icon = "book"

    index_view_class = GuestBookEntryIndexView
    add_view_class = GuestBookEntryCreateView
    edit_view_class = GuestBookEntryEditView
    delete_view_class = GuestBookEntryDeleteView

    form_fields = ["name", "email", "message", "is_visible"]
