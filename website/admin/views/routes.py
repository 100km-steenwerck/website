from wagtail.admin.ui.tables import UpdatedAtColumn
from wagtail.admin.views import generic
from wagtail.admin.viewsets.model import ModelViewSet

from website.models import Route


class IndexView(generic.IndexView):
    add_item_label = "Ajouter un parcours"
    list_display = ["name", UpdatedAtColumn(width=250)]


class CreateView(generic.CreateView):
    success_message = "Parcours « %(object)s » ajouté."
    error_message = "Le parcours ne peut être créé du fait d'erreurs."

    def get_breadcrumbs_items(self):
        items = super().get_breadcrumbs_items()
        items[-1]["label"] = "Nouveau parcours"
        return items


class EditView(generic.EditView):
    success_message = "Parcours « %(object)s » mis à jour."
    error_message = "Le parcours ne peut être enregistré du fait d'erreurs."


class DeleteView(generic.DeleteView):
    confirmation_message = "Êtes-vous sûr⋅e de vouloir supprimer ce parcours ?"
    success_message = "Parcours « %(object)s » supprimé."


class RouteViewSet(ModelViewSet):
    model = Route
    icon = "route"
    menu_label = "Parcours"

    index_view_class = IndexView
    add_view_class = CreateView
    edit_view_class = EditView
    delete_view_class = DeleteView
