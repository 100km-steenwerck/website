from django.utils.translation import ngettext

from wagtail.admin.views.bulk_action import BulkAction
from wagtail.permission_policies import ModelPermissionPolicy

from website.models import Registration


class RegistrationBulkAction(BulkAction):
    models = [Registration]
    permission_policy = ModelPermissionPolicy(Registration)


class DeleteBulkAction(RegistrationBulkAction):
    display_name = "Supprimer"
    aria_label = "Supprimer les inscriptions sélectionnées"
    action_type = "delete"
    template_name = "admin/registrations/bulk_actions/confirm_bulk_delete.html"
    action_priority = 100
    classes = {"serious"}

    def check_perm(self, obj):
        return self.permission_policy.user_has_permission_for_instance(
            self.request.user, "delete", obj
        )

    @classmethod
    def execute_action(cls, objects, **kwargs):
        num_parent_objects = len(objects)
        cls.get_default_model().objects.filter(
            pk__in=[obj.pk for obj in objects]
        ).delete()
        return num_parent_objects, 0

    def get_success_message(self, num_parent_objects, num_child_objects):
        return ngettext(
            "%(num_parent_objects)d inscription a été supprimée.",
            "%(num_parent_objects)d inscriptions ont été supprimées.",
            num_parent_objects,
        ) % {"num_parent_objects": num_parent_objects}
