from django.conf import settings
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.template.loader import render_to_string
from django.utils.functional import cached_property

from wagtail.admin.panels import FieldPanel, ObjectList
from wagtail.admin.ui.tables import (
    BulkActionsCheckboxColumn,
    Column,
    DateColumn,
    StatusTagColumn,
    TitleColumn,
)
from wagtail.admin.views import generic
from wagtail.admin.viewsets.model import ModelViewSet

from website.admin.filters import RegistrationFilterSet
from website.models import Registration


class IndexView(generic.IndexView):
    add_item_label = "Ajouter une inscription"
    filterset_class = RegistrationFilterSet
    search_fields = ["email", "first_name", "last_name"]
    template_name = "admin/registrations/index.html"

    def get_base_queryset(self):
        return super().get_base_queryset().select_related("race_edition__race")

    @cached_property
    def columns(self):
        def get_url(instance):
            return self.get_edit_url(instance)

        return [
            BulkActionsCheckboxColumn("bulk_actions", obj_type="registration"),
            self._get_title_column_class(TitleColumn)(
                "get_full_name",
                label="Personne",
                sort_key="last_name",
                get_url=get_url,
            ),
            Column("email", label="Adresse mail"),
            StatusTagColumn("get_status_display", label="Statut", width=110),
            DateColumn(
                "created_at",
                label="Date d'inscription",
                sort_key="created_at",
            ),
        ]


class CreateView(generic.CreateView):
    success_message = "Inscription « %(object)s » ajoutée."
    error_message = "L'inscription ne peut être créée du fait d'erreurs."

    panels = [
        FieldPanel("race_edition"),
    ] + Registration.identity_panels

    def get_panel(self):
        return ObjectList(self.panels).bind_to_model(self.model)

    def get_form_class(self):
        return self.panel.get_form_class()

    def get_success_url(self):
        return self.get_edit_url()

    def get_success_buttons(self):
        return None

    def get_breadcrumbs_items(self):
        items = super().get_breadcrumbs_items()
        items[-1]["label"] = "Nouvelle inscription"
        return items


class EditView(generic.EditView):
    success_message = "Inscription « %(object)s » mise à jour."
    error_message = "L'inscription ne peut être enregistrée du fait d'erreurs."

    def get_queryset(self):
        return super().get_queryset().select_related("race_edition__race")

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        # Stocke le statut initial de l'inscription
        self.initial_status = obj.status
        return obj

    def run_after_hook(self):
        if (
            self.object.email
            and self.object.status != self.initial_status
            and self.object.status == Registration.Status.VALIDATED
        ):
            self.send_validation_mail()

    def send_validation_mail(self):
        """Envoi le courriel de validation à la personne."""
        subject = (
            "Inscription validée pour la course %s"
            % self.object.race_edition.race
        )
        body = render_to_string(
            "registration/validation_email.txt",
            context={
                "registration": self.object,
                "race_edition": self.object.race_edition,
            },
        )

        self.object.email_person(
            subject,
            body,
            reply_to=settings.REGISTRATION_REPLY_TO,
        )


class DeleteView(generic.DeleteView):
    confirmation_message = (
        "Êtes-vous sûr⋅e de vouloir supprimer cette inscription ?"
    )
    success_message = "Inscription « %(object)s » supprimée."

    def get_queryset(self):
        return super().get_queryset().select_related("race_edition__race")


class RegistrationViewSet(ModelViewSet):
    model = Registration
    icon = "clipboard-user"

    index_view_class = IndexView
    add_view_class = CreateView
    edit_view_class = EditView
    delete_view_class = DeleteView

    ordering = ["-created_at"]

    def get_permissions_to_register(self):
        # N'exclue pas la permission 'view', utilisée pour voir les rapports
        content_type = ContentType.objects.get_for_model(self.model)
        return Permission.objects.filter(content_type=content_type)
