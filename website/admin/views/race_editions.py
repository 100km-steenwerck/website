from django.urls import reverse
from django.utils.functional import cached_property

from wagtail.admin.ui.tables import (
    BooleanColumn,
    Column,
    TitleColumn,
    UpdatedAtColumn,
)
from wagtail.admin.views import generic
from wagtail.admin.viewsets.model import ModelViewSet
from wagtail.admin.widgets.button import ListingButton

from website.models import RaceEdition


class IndexView(generic.IndexView):
    add_item_label = "Ajouter une édition"
    list_filter = ["race", "issue"]

    def get_base_queryset(self):
        return super().get_base_queryset().select_related("race")

    @cached_property
    def columns(self):
        def get_url(instance):
            return self.get_edit_url(instance)

        return [
            self._get_title_column_class(TitleColumn)(
                "get_issue_html_display",
                label="Édition",
                sort_key="issue",
                get_url=get_url,
            ),
            Column("race", label="Course", sort_key="race"),
            Column("start_at", label="Début de l'épreuve", sort_key="start_at"),
            BooleanColumn(
                "has_registration",
                label="Inscriptions",
                width=110,
            ),
            BooleanColumn(
                "is_current",
                label="En cours",
                width=110,
            ),
            UpdatedAtColumn(label="Mise à jour", width=250),
        ]

    def get_list_more_buttons(self, instance):
        buttons = super().get_list_more_buttons(instance)
        buttons.append(
            ListingButton(
                "Modifier la course",
                url=reverse(
                    "wagtailadmin_pages:edit",
                    args=(instance.race_id,),
                ),
                icon_name="person-walking",
                attrs={
                    "aria-label": "Modifier la page « {race} »".format(
                        race=instance.race,
                    )
                },
                priority=15,
            )
        )
        return buttons


class CreateView(generic.CreateView):
    success_message = "Édition « %(object)s » ajoutée."
    error_message = "L'édition ne peut être créée du fait d'erreurs."

    def get_breadcrumbs_items(self):
        items = super().get_breadcrumbs_items()
        items[-1]["label"] = "Nouvelle édition"
        return items


class EditView(generic.EditView):
    success_message = "Édition « %(object)s » mise à jour."
    error_message = "L'édition ne peut être enregistrée du fait d'erreurs."

    def get_queryset(self):
        return super().get_queryset().select_related("race")


class DeleteView(generic.DeleteView):
    confirmation_message = (
        "Êtes-vous sûr⋅e de vouloir supprimer cette édition ?"
    )
    success_message = "Édition « %(object)s » supprimée."

    def get_queryset(self):
        return super().get_queryset().select_related("race")


class RaceEditionViewSet(ModelViewSet):
    model = RaceEdition
    icon = "date"

    index_view_class = IndexView
    add_view_class = CreateView
    edit_view_class = EditView
    delete_view_class = DeleteView
