document.addEventListener('DOMContentLoaded', () => {
  const hasRegistrationInput = document.getElementById('id_has_registration');
  const regsitrationSettingsPanel = document.querySelector('[data-registration-settings]');
  const registrationSettingsToggle = regsitrationSettingsPanel.querySelector('button[data-panel-toggle]');

  const isRegistrationSettingsExpanded = () =>
    registrationSettingsToggle.getAttribute('aria-expanded') === 'true';

  const updateRegsitrationSettingsPanel = () => {
    if (hasRegistrationInput.checked && !isRegistrationSettingsExpanded()) {
      registrationSettingsToggle.click();
    } else if (!hasRegistrationInput.checked && isRegistrationSettingsExpanded()) {
      registrationSettingsToggle.click();
    }
  };

  // Définis l'état initial du panel
  updateRegsitrationSettingsPanel();

  hasRegistrationInput.addEventListener('change', updateRegsitrationSettingsPanel);
});
