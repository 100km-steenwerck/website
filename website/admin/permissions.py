from wagtail.permission_policies import ModelPermissionPolicy

from website.models import Registration

registration_permission_policy = ModelPermissionPolicy(Registration)
