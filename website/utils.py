from django.utils.formats import number_format


def get_age(birthdate, at_date):
    """Retourne l'âge à une date donnée."""
    return (
        at_date.year
        - birthdate.year
        - ((at_date.month, at_date.day) < (birthdate.month, birthdate.day))
    )


def normalize_email(email):
    """
    Normalise l'adresse mail en supprimant les espaces et en la mettant
    en minuscules.
    """
    return email.strip().lower() if email else ""


def price_format(value):
    """Formate un nombre comme un prix en euros."""
    if not value:
        return "0 €"
    return "{} €".format(number_format(value, decimal_pos=2))
