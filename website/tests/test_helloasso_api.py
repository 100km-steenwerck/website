import json
from unittest import mock

from django.test import override_settings

import pytest

TOKEN_RESPONSE = {
    "token_type": "bearer",
    "access_token": "a",
    "refresh_token": "b",
    "expires_in": 1800,
}


def mock_json_response(payload, requests=None):
    def fake_send(r, **kwargs):
        resp = mock.MagicMock()
        resp.status_code = 200
        if r.body and "grant_type=client_credentials" in str(r.body):
            resp.json = lambda: TOKEN_RESPONSE
        else:
            resp.json = lambda: payload
        if requests is not None:
            requests.append(r)
        return resp

    return fake_send


@pytest.fixture
def sandbox_client():
    from website.helloasso_api import SandboxClient

    return SandboxClient()


@pytest.fixture
def production_client():
    from website.helloasso_api import ProductionClient

    return ProductionClient()


class TestClient:
    def test_default_client(self):
        from website.helloasso_api import client

        assert client.base_url == "https://api.helloasso-sandbox.com/v5/"

    @override_settings(USE_HELLOASSO_PRODUCTION=True)
    def test_use_production_client(self):
        from website.helloasso_api import get_client

        assert get_client().base_url == "https://api.helloasso.com/v5/"

    def test_build_absolute_uri(self, production_client, rf):
        request = rf.get("/other-path/")
        assert production_client.build_absolute_uri("/some-path/", request) == (
            "http://testserver/some-path/"
        )

        assert production_client.build_absolute_uri("/some-path/") == (
            "http://127.0.0.1:8000/some-path/"
        )

    def test_sandbox_build_absolute_uri(self, sandbox_client, rf):
        request = rf.get("/other-path/")
        assert sandbox_client.build_absolute_uri("/some-path/", request) == (
            "https://testserver/some-path/"
        )

    def test_fetch_token_before_request(self, sandbox_client):
        requests = []

        sandbox_client.session.send = mock_json_response(
            {"response": "ok"},
            requests,
        )
        sandbox_client.get("forms")
        assert len(requests) == 2
        assert requests[0].url == sandbox_client.token_endpoint
        assert requests[0].body == (
            "grant_type=client_credentials&client_id=id&client_secret=secret"
        )
        assert requests[1].url == sandbox_client.get_url("forms")
        assert requests[1].headers["Authorization"] == "Bearer a"

    def test_init_checkout_intent(self, sandbox_client):
        requests = []

        sandbox_client.session.send = mock_json_response(
            {
                "id": 123,
                "redirectUrl": "https://example.org",
            },
            requests,
        )
        assert sandbox_client.init_checkout_intent(
            1000,
            "Frais d'inscription",
            "/checkout/cancel/",
            "/checkout/error/",
            "/checkout/return/",
            "Marie-Pierre",
            "D’agoberd",
            "marie-pierre@example.org",
        ) == (123, "https://example.org")

        data = json.loads(requests[1].body)
        assert data == {
            "totalAmount": 1000,
            "initialAmount": 1000,
            "itemName": "Frais d'inscription",
            "backUrl": "http://127.0.0.1:8000/checkout/cancel/",
            "errorUrl": "http://127.0.0.1:8000/checkout/error/",
            "returnUrl": "http://127.0.0.1:8000/checkout/return/",
            "containsDonation": False,
            "payer": {
                "firstName": "Marie-Pierre",
                "lastName": "Dagoberd",
                "email": "marie-pierre@example.org",
            },
        }

    def test_json_response_error(self, sandbox_client):
        from website.helloasso_api import APIResponseError

        sandbox_client.session.send = mock_json_response(
            {
                "errors": [
                    {
                        "code": "ArgumentInvalid",
                        "message": (
                            "Votre nom ne doit utiliser que l'alphabet latin"
                        ),
                    }
                ]
            }
        )
        with pytest.raises(APIResponseError) as excinfo:
            sandbox_client.init_checkout_intent(
                1000,
                "Frais d'inscription",
                "/checkout/cancel/",
                "/checkout/error/",
                "/checkout/return/",
                "Camille",
                "Dupond",
                "camille@example.org",
            )
        assert excinfo.value.error_list[0]["code"] == "ArgumentInvalid"

    @pytest.mark.parametrize(
        "value, result",
        [
            ("Jérôme", "Jérôme"),
            ("Marie-Pierre", "Marie-Pierre"),
            ("D’agoberd", "Dagoberd"),
        ],
    )
    def test_sanitize_string(self, sandbox_client, value, result):
        assert sandbox_client.sanitize_string(value) == result
