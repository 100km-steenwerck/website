from wagtail.blocks.struct_block import StructBlockValidationError

import pytest

from website.blocks import (
    LinkTargetBlock,
    LinkTargetBlockAdapter,
    RouteChooserBlock,
)

from .factories import RouteFactory


class TestRouteChooserBlock:
    def test_get_form_state(self):
        block = RouteChooserBlock()

        assert block.get_form_state(1) == ["1"]

    def test_form_response(self):
        block = RouteChooserBlock()
        routes = RouteFactory.create_batch(2)

        value = block.value_from_datadict({"pk": routes[1].pk}, {}, "pk")
        assert value == routes[1]

    @pytest.mark.parametrize("value", ("", None, "10"))
    def test_form_response_none(self, value):
        block = RouteChooserBlock()
        RouteFactory.create_batch(2)

        value = block.value_from_datadict({"pk": value}, {}, "pk")
        assert value is None

    def test_serialize(self):
        block = RouteChooserBlock()
        route = RouteFactory()

        assert block.get_prep_value(route) == route.id
        assert block.get_prep_value(None) is None

    def test_deserialize(self):
        block = RouteChooserBlock()
        route = RouteFactory()

        assert block.to_python(route.id) == route
        assert block.to_python(None) is None

    def test_field_queryset(self):
        block = RouteChooserBlock()
        routes = RouteFactory.create_batch(2)

        assert set(block.field.queryset) == set(routes)


class TestLinkTargetBlock:
    def test_required(self):
        block = LinkTargetBlock()
        assert block.required is True

        block = LinkTargetBlock(required=False)
        assert block.required is False

    def test_default_types(self):
        block = LinkTargetBlock()
        assert block.block_types == ["page", "url", "anchor"]
        assert list(block.child_blocks["type"].field.choices) == [
            ("", "---------"),
            ("page", "Page"),
            ("url", "Lien externe"),
            ("anchor", "Lien d'ancrage"),
        ]

    def test_clean_required(self):
        block = LinkTargetBlock()

        with pytest.raises(StructBlockValidationError) as excinfo:
            block.clean({})
        assert set(excinfo.value.block_errors.keys()) == {"type"}
        assert excinfo.value.block_errors["type"].code == "required"

        with pytest.raises(StructBlockValidationError) as excinfo:
            block.clean({"type": "url", "anchor": "#"})
        assert set(excinfo.value.block_errors.keys()) == {"url"}
        assert excinfo.value.block_errors["url"].code == "required"

    def test_clean_invalid(self):
        block = LinkTargetBlock()

        with pytest.raises(StructBlockValidationError) as excinfo:
            block.clean({"type": "url", "url": "pouet"})
        assert set(excinfo.value.block_errors.keys()) == {"url"}
        assert excinfo.value.block_errors["url"].messages == [
            "Saisissez une URL valide."
        ]

    def test_clean_struct_value(self):
        block = LinkTargetBlock()

        struct_value = block.clean(
            {
                "type": "url",
                "anchor": "#pouet",
                "url": "http://example.org",
            }
        )
        assert struct_value.get("type") == "url"
        assert "anchor" not in struct_value
        assert struct_value.get("url") == "http://example.org"
        assert struct_value.url == "http://example.org"

    def test_clean_struct_value_none(self):
        block = LinkTargetBlock(required=False)

        struct_value = block.clean({})
        assert struct_value.url == ""

    def test_clean_struct_value_page(self, guest_book_page):
        block = LinkTargetBlock()

        struct_value = block.clean(
            {
                "type": "page",
                "page": guest_book_page.pk,
            }
        )
        assert struct_value.get("type") == "page"
        assert struct_value.get("page").pk == guest_book_page.pk
        assert struct_value.url == guest_book_page.url

    def test_adapter(self):
        block = LinkTargetBlock()
        block.set_name("test_link")

        meta = LinkTargetBlockAdapter().js_args(block)[2]
        assert "formTemplate" in meta
        assert meta["required"] is True
        assert meta["blockTypes"] == block.block_types
