from decimal import Decimal

from wagtail.models import Page

import pytest
from pytest_factoryboy import LazyFixture, register
from wagtail_factories import (
    CollectionFactory,
    DocumentFactory,
    ImageChooserBlockFactory,
    ImageFactory,
)

from .factories import (
    GuestBookEntryFactory,
    GuestBookPageFactory,
    HomePageFactory,
    RaceEditionFactory,
    RaceIndexPageFactory,
    RacePageFactory,
    RichTextBlockFactory,
)
from .utils import aware_datetime

register(CollectionFactory)
register(DocumentFactory)
register(ImageFactory)
register(ImageChooserBlockFactory)
register(RichTextBlockFactory)


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


@pytest.fixture
def root_page():
    return Page.objects.filter(sites_rooted_here__is_default_site=True).get()


# PAGES DE BASE
# ------------------------------------------------------------------------------

register(HomePageFactory, parent=LazyFixture("root_page"))

# LIVRE D'OR
# ------------------------------------------------------------------------------

register(GuestBookEntryFactory)
register(GuestBookPageFactory, parent=LazyFixture("home_page"))

# COURSES
# ------------------------------------------------------------------------------

register(RaceEditionFactory)
register(RaceIndexPageFactory, parent=LazyFixture("home_page"))
register(RacePageFactory, parent=LazyFixture("race_index_page"))

register(
    RacePageFactory,
    "open2024_race_page",
    parent=LazyFixture("race_index_page"),
    title="100 km Open 24h",
)
register(
    RaceEditionFactory,
    "open2024_race_edition",
    race=LazyFixture("open2024_race_page"),
    issue=47,
    start_at=aware_datetime(2024, 5, 8, 19),
    is_current=True,
    has_registration=True,
    registration_price=Decimal("30"),
    registration_opening_at=aware_datetime(2024, 2, 15),
    registration_ending_at=aware_datetime(2024, 5, 1),
    registration_required_age=18,
)
