import datetime

from django.utils import timezone

import pytest


def aware_datetime(*args):
    """
    Retourne un objet `datetime.datetime` "aware" avec les arguments donnés.
    """
    return timezone.make_aware(datetime.datetime(*args))


class ViewMixin:
    """
    Mixin ajoutant des facilitées pour faire des requêtes avec Webtest, à
    appliquer aux classes définissant des tests sur les vues.
    """

    #: L'URL à utiliser par défaut pour les requêtes.
    url = None

    #: L'utilisateur à positionner par défaut dans les requêtes.
    user = None

    #: Détermine s'il faut activer la protection CSRF.
    csrf_checks = True

    @pytest.fixture(autouse=True)
    def setup_django_app(self, django_app_factory):
        self.django_app = django_app_factory(csrf_checks=self.csrf_checks)

    def _query(self, method, url=None, **kwargs):
        """
        Exécute la requête de type `method` vers `url`. Pour les autres
        arguments disponibles, voir `webtest.app.TestApp.get`.
        """
        kwargs.setdefault("user", self.user)
        return getattr(self.django_app, method)(url or self.url, **kwargs)

    def get(self, url=None, **kwargs):
        return self._query("get", url, **kwargs)

    def post(self, url=None, **kwargs):
        return self._query("post", url, **kwargs)

    def submit(self, form, name=None, extra_data={}, **kwargs):
        """
        Envoie le formulaire `webtest.forms.Form` donné avec des données
        supplémentaires optionnelles et retourne la réponse.
        """
        fields = dict(form.submit_fields(name), **extra_data)
        if form.method.upper() != "GET":
            kwargs.setdefault("content_type", form.enctype)
        extra_environ = kwargs.setdefault("extra_environ", {})
        extra_environ.setdefault("HTTP_REFERER", str(form.response.request.url))
        return form.response.goto(
            form.action,
            method=form.method,
            params=fields,
            **kwargs,
        )
