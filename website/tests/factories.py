from decimal import Decimal

from django.contrib.auth.models import Group, Permission

from wagtail.blocks import RichTextBlock

import factory
import wagtail_factories
from factory.django import DjangoModelFactory, FileField
from faker import Factory as FakerFactory
from wagtail_factories import (
    DocumentFactory,
    ImageChooserBlockFactory,
    ListBlockFactory,
)
from wagtail_factories.blocks import BlockFactory, ChooserBlockFactory

from .. import blocks, models
from .utils import aware_datetime

faker = FakerFactory.create(locale="fr_FR")


class UserFactory(DjangoModelFactory):
    class Meta:
        model = models.User
        skip_postgeneration_save = True

    email = factory.Sequence(lambda x: f"user{x}@example.org")
    first_name = factory.LazyFunction(lambda: faker.first_name())
    last_name = factory.LazyFunction(lambda: faker.last_name())

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # Passe par UserManager afin de définir correctement le mot de passe
        return cls._get_manager(model_class).create_user(*args, **kwargs)

    @factory.post_generation
    def groups(obj, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for group in extracted:
                obj.groups.add(group)

    @factory.post_generation
    def user_permissions(obj, create, extracted, codenames=None, **kwargs):
        if not create:
            return
        if codenames:
            for perm in Permission.objects.filter(codename__in=codenames):
                obj.user_permissions.add(perm)
        if extracted:
            for perm in extracted:
                obj.user_permissions.add(perm)


class GroupFactory(DjangoModelFactory):
    class Meta:
        model = Group
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda x: "Groupe #{0}".format(x))

    @factory.post_generation
    def permissions(obj, create, extracted, codenames=None, **kwargs):
        if not create:
            return
        if codenames:
            for perm in Permission.objects.filter(codename__in=codenames):
                obj.permissions.add(perm)
        if extracted:
            for perm in extracted:
                obj.permissions.add(perm)


# BLOCS
# ------------------------------------------------------------------------------


class RouteChooserBlockFactory(ChooserBlockFactory):
    class Meta:
        model = blocks.RouteChooserBlock

    route = factory.SubFactory("website.tests.factories.RouteFactory")

    @classmethod
    def _build(cls, model_class, route):
        return route

    @classmethod
    def _create(cls, model_class, route):
        return route


class RichTextBlockFactory(BlockFactory):
    class Meta:
        model = RichTextBlock


class LinkTargetBlockFactory(wagtail_factories.StructBlockFactory):
    class Meta:
        model = blocks.LinkTargetBlock


class ImageBlockFactory(wagtail_factories.StructBlockFactory):
    class Meta:
        model = blocks.ImageBlock

    image = factory.SubFactory(ImageChooserBlockFactory)


class TableBlockFactory(BlockFactory):
    class Meta:
        model = blocks.TableBlock

    @classmethod
    def _construct_block(cls, block_class, *args, **kwargs):
        if kwargs.get("value"):
            # Le format attendu de la valeur est comme celui définis en base
            # de données, on le convertis en `TypedTable` pour la suite
            value = block_class().to_python(kwargs["value"])
            return block_class().clean(value)
        return block_class().get_default()


class ImagesGridBlockFactory(wagtail_factories.StructBlockFactory):
    class Meta:
        model = blocks.ImagesGridBlock

    size = blocks.ImagesGridBlock.Sizes.LOGO
    images = ListBlockFactory(ImageBlockFactory)


class AlertBlockFactory(wagtail_factories.StructBlockFactory):
    class Meta:
        model = blocks.AlertBlock

    message = factory.SubFactory(RichTextBlockFactory)
    level = "warning"


class RouteMapBlockFactory(wagtail_factories.StructBlockFactory):
    class Meta:
        model = blocks.RouteMapBlock

    route = factory.SubFactory(RouteChooserBlockFactory)


# PAGES DE BASE
# ------------------------------------------------------------------------------


class PageNavigationFactory(DjangoModelFactory):
    class Meta:
        model = models.PageNavigation

    menu = "main"


class HomePageFactory(wagtail_factories.PageFactory):
    class Meta:
        model = models.HomePage

    background_image = factory.SubFactory(
        wagtail_factories.ImageChooserBlockFactory
    )

    is_alert_visible = True
    alert_message = "Les inscriptions sont ouvertes !"


class ContentPageFactory(wagtail_factories.PageFactory):
    class Meta:
        model = models.ContentPage

    body = wagtail_factories.StreamFieldFactory(
        {
            "title_block": factory.SubFactory(
                wagtail_factories.CharBlockFactory
            ),
            "paragraph_block": factory.SubFactory(RichTextBlockFactory),
            "image_block": factory.SubFactory(ImageBlockFactory),
            "alert_block": factory.SubFactory(AlertBlockFactory),
            "route_map_block": factory.SubFactory(RouteMapBlockFactory),
            "table_block": factory.SubFactory(TableBlockFactory),
            "images_grid_block": factory.SubFactory(ImagesGridBlockFactory),
        }
    )

    banner_image = factory.SubFactory(
        wagtail_factories.ImageChooserBlockFactory
    )
    introduction = factory.SubFactory(RichTextBlockFactory)


class IndexPageFactory(wagtail_factories.PageFactory):
    class Meta:
        model = models.IndexPage

    introduction = factory.SubFactory(RichTextBlockFactory)


# LIVRE D'OR
# ------------------------------------------------------------------------------


class GuestBookEntryFactory(DjangoModelFactory):
    class Meta:
        model = models.GuestBookEntry

    name = factory.LazyFunction(lambda: faker.name())
    email = factory.LazyFunction(lambda: faker.email())
    message = "Coucou,\nVoici mon message dans le livre d'or, merci !"


class GuestBookPageFactory(wagtail_factories.PageFactory):
    class Meta:
        model = models.GuestBookPage

    title = "Livre d'or"
    introduction = "Bienvenue sur le livre d'or."
    per_page = 15


# COURSES
# ------------------------------------------------------------------------------


class RacePageFactory(ContentPageFactory):
    class Meta:
        model = models.RacePage


class RaceIndexPageFactory(wagtail_factories.PageFactory):
    class Meta:
        model = models.RaceIndexPage


class RaceEditionFactory(DjangoModelFactory):
    class Meta:
        model = models.RaceEdition

    race = factory.SubFactory(RacePageFactory)
    issue = factory.Sequence(lambda x: x + 1)

    distance = Decimal("100")
    start_at = aware_datetime(2024, 5, 8, 19)

    is_current = False
    race_rules = factory.SubFactory(DocumentFactory)
    sale_conditions = factory.SubFactory(DocumentFactory)

    has_registration = False
    registration_price = None
    registration_opening_at = None
    registration_ending_at = None
    registration_required_age = None


# PARCOURS
# ------------------------------------------------------------------------------


class RouteLoopFactory(DjangoModelFactory):
    class Meta:
        model = models.RouteLoop

    name = factory.Sequence(lambda x: f"Boucle #{x}")
    color = models.RouteLoop.Colors.PINK
    distance = Decimal("12.256")
    gpx_file = FileField(filename="boucle.gpx")

    route = factory.SubFactory(
        "website.tests.factories.RouteFactory",
        loops=None,
    )


class RouteFactory(DjangoModelFactory):
    class Meta:
        model = models.Route
        skip_postgeneration_save = True

    name = factory.Sequence(lambda x: f"Parcours #{x}")

    loops = factory.RelatedFactoryList(
        RouteLoopFactory,
        factory_related_name="route",
        size=2,
    )
