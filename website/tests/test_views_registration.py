import datetime

from django.contrib.messages import ERROR as ERROR_LEVEL

import pytest
import time_machine

from .utils import ViewMixin


class TestRegistrationViews(ViewMixin):
    def get_register_url(self, page, edition_issue):
        return f"{page.url}edition/{edition_issue}/register/"

    def test_edition_issue_unknown(self, open2024_race_page):
        url = self.get_register_url(open2024_race_page, 75)

        html = self.get(url, status=404).html
        assert html.select_one("main h2 ~ p").text.strip() == (
            "Il n'est soit pas possible de s'inscrire à l'édition de la course "
            "demandée, ou alors celle-ci n'existe pas."
        )

    @time_machine.travel(datetime.date(2024, 3, 1))
    @pytest.mark.parametrize("open2024_race_edition__has_registration", [False])
    def test_edition_without_registration(
        self, open2024_race_page, open2024_race_edition
    ):
        url = self.get_register_url(
            open2024_race_page, open2024_race_edition.issue
        )

        html = self.get(url, status=404).html
        assert html.select_one("main h2 ~ p").text.strip() == (
            "Il n'est soit pas possible de s'inscrire à l'édition de la course "
            "demandée, ou alors celle-ci n'existe pas."
        )

    @time_machine.travel(datetime.date(2024, 6, 1))
    def test_registration_ended(
        self, open2024_race_page, open2024_race_edition
    ):
        url = self.get_register_url(
            open2024_race_page, open2024_race_edition.issue
        )

        response = self.get(url, status=302)
        assert response.location == open2024_race_page.url

        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == ERROR_LEVEL
        assert messages[0].message == (
            "Les inscriptions pour la 47<sup>e</sup> édition sont terminées."
        )

    @time_machine.travel(datetime.date(2024, 2, 1))
    def test_registration_not_yet_opened(
        self, open2024_race_page, open2024_race_edition
    ):
        url = self.get_register_url(
            open2024_race_page, open2024_race_edition.issue
        )

        response = self.get(url, status=302)
        assert response.location == open2024_race_page.url

        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == ERROR_LEVEL
        assert messages[0].message == (
            "Les inscriptions pour la 47<sup>e</sup> édition ne sont pas "
            "encore ouvertes."
        )

    @time_machine.travel(datetime.date(2024, 3, 1))
    def test_register_init(self, open2024_race_page, open2024_race_edition):
        url = self.get_register_url(
            open2024_race_page, open2024_race_edition.issue
        )
        identity_step_url = url + "identity/"
        session_key = "edition_registration_%d" % open2024_race_edition.pk

        response = self.get(url, status=302)
        assert response.location == identity_step_url

        response = response.follow()
        assert response.client.session[session_key] == {}
        form = response.form
        assert form.action == identity_step_url
        assert form.html.find("button", type="submit").text.strip() == (
            "Continuer"
        )
