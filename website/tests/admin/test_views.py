from django.contrib.auth.models import Permission
from django.urls import reverse

from .utils import AdminViewMixin


class TestWagtailGroupPermissionsView(AdminViewMixin):
    add_url = reverse("wagtailusers_groups:add")

    def test_custom_permissions(self):
        html = self.get(self.add_url).html

        row = html.find("h3", string="Message du livre d'or").parent.parent
        permissions = row.select('input[name="permissions"]')
        assert len(permissions) == 3
        assert int(permissions[0].attrs["value"]) == (
            Permission.objects.get(codename="add_guestbookentry").pk
        )
