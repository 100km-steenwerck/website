import datetime
from html import escape

import time_machine

from website.models import GuestBookEntry

from .utils import AdminViewSetMixin


class TestGuestBookEntryViews(AdminViewSetMixin):
    namespace = "admin_guestbook"

    def test_index(self, guest_book_entry_factory):
        guest_book_entry_factory.create_batch(2)
        invisible_entry = guest_book_entry_factory(is_visible=False)

        html = self.get(self.index_url, status=200).html
        assert html.find("a", href=self.add_url).text.strip() == (
            "Ajouter un message"
        )

        rows = html.select("#listing-results tbody tr")
        assert len(rows) == 3
        # - les retours à la ligne du message sont formatés
        assert "Coucou,<br/>Voici" in str(rows[1].select("td")[2])
        # - les actions sont affichées en 2e colonne
        actions_links = rows[1].select("td")[1].select("ul a")
        assert len(actions_links) == 3
        assert actions_links[0].text.strip() == "Modifier"
        assert actions_links[1].text.strip() == "Copier"
        assert actions_links[2].text.strip() == "Supprimer"

        # Filtre avec les messages non visibles
        html = self.get("%s?is_visible=false" % self.index_url).html
        rows = html.select("#listing-results tbody tr")
        assert len(rows) == 1
        assert rows[0].select("td")[1].select("a")[0].text.strip() == (
            invisible_entry.name
        )

    @time_machine.travel(datetime.date(2023, 11, 17))
    def test_add(self):
        form = self.get(self.add_url, status=200).forms[0]
        assert form.action == self.add_url

        form["name"] = "Camille Dupont"
        form["message"] = "Voici mon message."
        response = form.submit(status=302)
        assert response["location"] == self.index_url
        assert (
            escape("Message « Camille Dupont le 17 novembre 2023 » ajouté.")
            in response.follow()
        )

        GuestBookEntry.objects.get(name="Camille Dupont")

    def test_edit(self, guest_book_entry):
        url = self.get_edit_url(guest_book_entry)

        form = self.get(url).forms[0]
        form["is_visible"] = False
        response = form.submit(status=302)
        assert response["location"] == self.index_url
        assert (
            escape("Message « %s » mis à jour." % str(guest_book_entry))
            in response.follow()
        )

        guest_book_entry.refresh_from_db()
        guest_book_entry.is_visible is False

    def test_delete(self, guest_book_entry):
        url = self.get_delete_url(guest_book_entry)

        form = self.get(url, status=200).forms[0]
        assert form.action == url

        form.submit().follow(status=200)
        assert not GuestBookEntry.objects.filter(
            pk=guest_book_entry.pk
        ).exists()
