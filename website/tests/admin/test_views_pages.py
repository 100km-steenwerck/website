from django.urls import reverse

from wagtail.test.utils.form_data import nested_form_data, streamfield

from ..factories import RaceEditionFactory, RacePageFactory
from .utils import AdminViewMixin


class TestHomePage(AdminViewMixin):
    def test_edit_alert_is_visible(self, home_page):
        url = reverse(
            "wagtailadmin_pages:edit",
            kwargs={"page_id": home_page.pk},
        )

        form = self.get(url, status=200).forms[1]
        form["is_alert_visible"] = True
        form["alert_message"] = ""

        response = form.submit(status=200)
        assert response.context["form"].errors == {
            "alert_message": [
                "Champs requis pour rendre visible le bandeau d'alerte."
            ]
        }

        form = response.forms[1]
        form["is_alert_visible"] = True
        form["alert_message"] = "Un message"
        form.submit("action-publish", status=302).follow()

        home_page.refresh_from_db()
        assert home_page.alert_message == "Un message"


class TestRacePage(AdminViewMixin):
    def test_add_current_edition(self, race_index_page):
        url = reverse(
            "wagtailadmin_pages:add",
            kwargs={
                "content_type_app_name": "website",
                "content_type_model_name": "racepage",
                "parent_page_id": race_index_page.pk,
            },
        )

        form = self.get(url, status=200).forms[0]
        assert form.get("current_edition").options == [
            ("", True, "Aucune"),
        ]

    def test_set_current_edition(self, race_index_page):
        race_page = RacePageFactory(
            title="100km",
            parent=race_index_page,
            body__0__title_block__value="La course",
        )
        RaceEditionFactory.reset_sequence()
        race_editions = RaceEditionFactory.create_batch(2, race=race_page)

        assert race_page.editions.current() is None

        url = reverse(
            "wagtailadmin_pages:edit",
            kwargs={"page_id": race_page.pk},
        )

        form = self.get(url, status=200).forms[1]
        assert form.get("current_edition").options == [
            ("", True, "Aucune"),
            (str(race_editions[1].pk), False, "100km - 2e édition"),
            (str(race_editions[0].pk), False, "100km - 1e édition"),
        ]

        # Définis une édition en cours
        form["current_edition"] = race_editions[1].pk

        self.submit(
            form,
            "action-publish",
            extra_data=nested_form_data(
                {
                    "body": streamfield(
                        [
                            ("title_block", "La course"),
                        ]
                    ),
                }
            ),
            status=302,
        ).follow()

        race_editions[0].refresh_from_db()
        assert race_editions[0].is_current is False
        race_editions[1].refresh_from_db()
        assert race_editions[1].is_current is True
        assert race_page.editions.current() == race_editions[1]

    def test_reset_current_edition(self, race_index_page):
        race_page = RacePageFactory(
            title="100km",
            parent=race_index_page,
            body__0__title_block__value="La course",
        )
        race_editions = [
            RaceEditionFactory(race=race_page, issue=1),
            RaceEditionFactory(race=race_page, issue=2, is_current=True),
        ]

        assert race_page.editions.current() == race_editions[1]

        url = reverse(
            "wagtailadmin_pages:edit",
            kwargs={"page_id": race_page.pk},
        )

        form = self.get(url, status=200).forms[1]
        assert form.get("current_edition").options == [
            ("", False, "Aucune"),
            (str(race_editions[1].pk), True, "100km - 2e édition"),
            (str(race_editions[0].pk), False, "100km - 1e édition"),
        ]

        # Retire l'édition en cours
        form["current_edition"] = ""

        self.submit(
            form,
            "action-publish",
            extra_data=nested_form_data(
                {
                    "body": streamfield(
                        [
                            ("title_block", "La course"),
                        ]
                    ),
                }
            ),
            status=302,
        ).follow()

        race_editions[0].refresh_from_db()
        assert race_editions[0].is_current is False
        race_editions[1].refresh_from_db()
        assert race_editions[1].is_current is False
        assert race_page.editions.current() is None
