import datetime
from decimal import Decimal
from html import escape

from django.urls import reverse

from wagtail.test.utils.form_data import inline_formset, nested_form_data

import time_machine
from wagtail_factories import DocumentFactory

from website.models import RaceEdition

from ..factories import RaceEditionFactory, RacePageFactory
from ..utils import aware_datetime
from .utils import AdminViewSetMixin


class TestRaceEditionViews(AdminViewSetMixin):
    namespace = "admin_raceeditions"

    @time_machine.travel(datetime.date(2024, 1, 1))
    def test_index(self, race_index_page):
        race1 = RacePageFactory(title="100km", parent=race_index_page)
        RaceEditionFactory(
            race=race1,
            issue=47,
            is_current=True,
            has_registration=True,
            registration_price=Decimal("30"),
            registration_opening_at=aware_datetime(2024, 2, 1),
            registration_ending_at=aware_datetime(2024, 5, 1),
        )
        RaceEditionFactory(
            race=race1,
            issue=46,
            start_at=aware_datetime(2023, 5, 18, 19),
        )
        race2 = RacePageFactory(title="Sans édition", parent=race_index_page)

        html = self.get(self.index_url, status=200).html
        assert html.find("a", href=self.add_url).text.strip() == (
            "Ajouter une édition"
        )

        rows = html.select("#listing-results tbody tr")
        assert len(rows) == 2
        # - le numéro d'édition
        assert rows[0].select("td")[0].select("a")[0].text.strip() == (
            "47e édition"
        )
        assert rows[1].select("td")[0].select("a")[0].text.strip() == (
            "46e édition"
        )
        # - la course
        assert rows[0].select("td")[1].text.strip() == "100km"
        assert rows[1].select("td")[1].text.strip() == "100km"
        # - le début de l'épreuve
        assert rows[0].select("td")[2].text.strip() == "8 mai 2024 19:00"
        assert rows[1].select("td")[2].text.strip() == "18 mai 2023 19:00"
        # - si les inscriptions sont possibles
        assert rows[0].select("td")[3].text.strip() == "True"
        assert rows[1].select("td")[3].text.strip() == "False"
        # - si c'est l'édition en cours
        assert rows[0].select("td")[4].text.strip() == "True"
        assert rows[1].select("td")[4].text.strip() == "False"
        # - les actions sont affichées en 1e colonne
        actions_links = rows[1].select("td")[0].select("ul a")
        assert len(actions_links) == 4
        assert actions_links[0].text.strip() == "Modifier"
        assert actions_links[1].text.strip() == "Modifier la course"
        assert actions_links[1].attrs["href"] == reverse(
            "wagtailadmin_pages:edit", args=(race1.pk,)
        )
        assert actions_links[2].text.strip() == "Copier"
        assert actions_links[3].text.strip() == "Supprimer"

        # Filtre sur la course
        html = self.get("%s?race=%d" % (self.index_url, race2.pk)).html
        rows = html.select("#listing-results tbody tr")
        assert len(rows) == 0

        # Filtre sur l'année
        html = self.get("%s?issue=46" % self.index_url).html
        rows = html.select("#listing-results tbody tr")
        assert len(rows) == 1
        assert rows[0].select("td")[0].select("a")[0].text.strip() == (
            "46e édition"
        )

    @time_machine.travel(datetime.date(2024, 1, 1))
    def test_add(self):
        race = RacePageFactory(title="100km")

        form = self.get(self.add_url, status=200).forms[0]
        assert form.action == self.add_url

        form["race"] = race.pk
        form["issue"] = "47"
        form["start_at"] = "2024-05-08 19:00"
        form["distance"] = "100"
        response = form.submit(status=302)
        assert response["location"] == self.index_url
        assert (
            escape("Édition « 100km - 47e édition » ajoutée.")
            in response.follow()
        )

        RaceEdition.objects.get(
            race=race.pk, is_current=False, has_registration=False
        )

    @time_machine.travel(datetime.date(2024, 1, 1))
    def test_edit(self):
        edition = RaceEditionFactory(
            race=RacePageFactory(title="100km"),
            issue=47,
            race_rules=None,
            sale_conditions=None,
        )
        url = self.get_edit_url(edition)

        form = self.get(url).forms[0]
        form["has_registration"] = True
        response = form.submit(status=200)
        assert set(response.context["form"].errors.keys()) == {
            "race_rules",
            "sale_conditions",
            "registration_price",
            "registration_opening_at",
            "registration_ending_at",
        }

        form["race_rules"] = DocumentFactory().pk
        form["sale_conditions"] = DocumentFactory().pk
        form["registration_price"] = "30"
        form["registration_opening_at"] = "2024-02-01 00:00"
        form["registration_ending_at"] = "2024-05-01 00:00"
        response = self.submit(
            form,
            extra_data=nested_form_data(
                {
                    "registration_options": inline_formset(
                        [
                            {"name": "Accompagnement", "price": "5"},
                            {"name": "Gobelet", "price": "1.5"},
                        ]
                    ),
                }
            ),
            status=302,
        )
        assert response["location"] == self.index_url
        assert (
            escape("Édition « 100km - 47e édition » mise à jour.")
            in response.follow()
        )

        edition.refresh_from_db()
        assert edition.has_registration is True
        options = edition.registration_options.all()
        assert len(options) == 2
        assert options[0].price == Decimal("5.00")
        assert options[1].price == Decimal("1.50")

    @time_machine.travel(datetime.date(2024, 1, 1))
    def test_registration_ending_before_opening(self):
        edition = RaceEditionFactory()
        url = self.get_edit_url(edition)

        form = self.get(url).forms[0]
        form["has_registration"] = True
        form["race_rules"] = DocumentFactory().pk
        form["sale_conditions"] = DocumentFactory().pk
        form["registration_price"] = "30"
        form["registration_opening_at"] = "2024-05-01 00:00"
        form["registration_ending_at"] = "2024-02-01 00:00"
        response = form.submit(status=200)
        assert response.context["form"].errors == {
            "registration_ending_at": [
                "Cette date doit être après celle d'ouverture.",
            ],
        }

    def test_delete(self):
        edition = RaceEditionFactory()
        url = self.get_delete_url(edition)

        form = self.get(url, status=200).forms[0]
        assert form.action == url

        form.submit().follow(status=200)
        assert not RaceEdition.objects.filter(pk=edition.pk).exists()
