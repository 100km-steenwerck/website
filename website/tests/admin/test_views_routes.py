from html import escape

from wagtail.test.utils.form_data import inline_formset, nested_form_data

from webtest import Upload

from website.models import Route, RouteLoop

from ..factories import RouteFactory
from .utils import AdminViewSetMixin

GPX_DATA = b"""
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="byHand" version="1.1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
</gpx>
"""  # noqa: E501


class TestRouteViews(AdminViewSetMixin):
    namespace = "admin_routes"

    def test_index(self):
        RouteFactory.reset_sequence()
        routes = RouteFactory.create_batch(2)

        html = self.get(self.index_url, status=200).html
        assert html.find("a", href=self.add_url).text.strip() == (
            "Ajouter un parcours"
        )
        rows = html.select("#listing-results tbody tr")
        assert len(rows) == 2
        assert rows[0].select("td")[0].select("a")[0].text.strip() == (
            routes[1].name
        )
        assert rows[1].select("td")[0].select("a")[0].text.strip() == (
            routes[0].name
        )

    def test_add(self):
        form = self.get(self.add_url, status=200).forms[0]
        assert form.action == self.add_url

        data = nested_form_data(
            {
                "csrfmiddlewaretoken": form.get("csrfmiddlewaretoken").value,
                "name": "100km",
                "loops": inline_formset(
                    [
                        {
                            "name": "Départ",
                            "color": RouteLoop.Colors.PINK.value,
                            "distance": "3.578",
                            "gpx_file": Upload("boucle.gpx", GPX_DATA),
                        },
                    ]
                ),
            }
        )
        response = self.post(self.add_url, params=data, status=302)
        assert response["location"] == self.index_url
        assert escape("Parcours « 100km » ajouté.") in response.follow()

        loops = Route.objects.get(name="100km").loops.all()
        assert len(loops) == 1
        assert loops[0].name == "Départ"

    def test_gpx_file_invalid_extension(self):
        form = self.get(self.add_url, status=200).forms[0]

        data = nested_form_data(
            {
                "csrfmiddlewaretoken": form.get("csrfmiddlewaretoken").value,
                "name": "100km",
                "loops": inline_formset(
                    [
                        {
                            "name": "Départ",
                            "color": RouteLoop.Colors.PINK.value,
                            "distance": "3.578",
                            "gpx_file": Upload("boucle.xml", GPX_DATA),
                        },
                    ]
                ),
            }
        )
        response = self.post(self.add_url, params=data, status=200)
        assert response.context["form"].formsets["loops"].errors[0] == {
            "gpx_file": ["Format de fichier non autorisé."]
        }

    def test_edit(self):
        route = RouteFactory()
        url = self.get_edit_url(route)

        form = self.get(url).forms[0]
        form["loops-0-name"] = "Départ"
        response = form.submit(status=302)
        assert response["location"] == self.index_url
        assert (
            escape("Parcours « %s » mis à jour." % str(route))
            in response.follow()
        )

        route.refresh_from_db()
        route.loops.get(name="Départ")

    def test_delete(self):
        route = RouteFactory()
        url = self.get_delete_url(route)

        form = self.get(url, status=200).forms[0]
        assert form.action == url

        form.submit().follow(status=200)
        assert not Route.objects.filter(pk=route.pk).exists()
        assert not RouteLoop.objects.exists()
