from ..models import User
from .factories import UserFactory


class TestUserManager:
    def test_create_user(self):
        user = User.objects.create_user("mail@example.org")
        assert user.is_staff is False
        assert user.is_superuser is False
        assert User.objects.get() == user

    def test_create_superuser(self):
        admin = User.objects.create_superuser("mail@example.org", "pass")
        assert admin.is_staff is True
        assert admin.is_superuser is True
        assert User.objects.get() == admin


class TestUser:
    def test_is_staff(self):
        admin = UserFactory(is_superuser=True)
        assert admin.is_staff is True

        staff = UserFactory(user_permissions__codenames=["access_admin"])
        assert staff.is_staff is True

        user = UserFactory()
        assert user.is_staff is False

    def test_short_and_full_name(self):
        user = User(email="mail@example.org")
        assert user.get_short_name() == ""
        assert user.get_full_name() == ""

        user.last_name = "Dupont"
        assert user.get_short_name() == ""
        assert user.get_full_name() == "Dupont"

        user.first_name = "Camille"
        assert user.get_short_name() == "Camille"
        assert user.get_full_name() == "Camille Dupont"

        assert str(user) == "Camille Dupont"

    def test_clean_email(self):
        user = UserFactory.build(email="Adresse@Example.Org")
        user.clean()
        assert user.email == "adresse@example.org"

    def test_email_user(self, mailoutbox):
        user = UserFactory.build()
        user.email_user("sujet", "body")
        assert len(mailoutbox) == 1
