import json

from wagtail.rich_text import RichText

import pytest
from wagtail_factories import ImageFactory

from website.blocks import ImagesGridBlock

from .factories import (
    ContentPageFactory,
    GuestBookPageFactory,
    IndexPageFactory,
    PageNavigationFactory,
    RouteFactory,
    RouteLoopFactory,
)
from .utils import ViewMixin


class TestPageNavigation(ViewMixin):
    def test_main_menu(self, home_page):
        url = home_page.get_url()

        page1 = IndexPageFactory(
            parent=home_page,
            title="Infos pratiques",
        )
        subpage11 = ContentPageFactory(parent=page1, title="Le parcours")
        subpage12 = ContentPageFactory(parent=page1, title="Hébergement")

        page2 = ContentPageFactory(parent=home_page, title="Être bénévole")
        ContentPageFactory(parent=page2, show_in_menus=False)

        page3 = ContentPageFactory(parent=home_page, title="Les courses")

        page4 = ContentPageFactory(parent=home_page, show_in_menus=False)

        PageNavigationFactory(page=page2, order=2)
        PageNavigationFactory(page=page3, order=3)
        PageNavigationFactory(page=page1, order=1)
        PageNavigationFactory(page=page4)

        html = self.get(url, status=200).html
        main_menus = html.select('nav[aria-label="Menu principal"]')
        assert len(main_menus) == 2
        assert main_menus[1].parent.attrs["id"] == "mobile-menu"

        items = main_menus[0].find("ul").find_all("li", recursive=False)
        assert len(items) == 3

        item1 = items[0].find("a")
        assert item1.text.strip() == page1.title
        assert item1.attrs["href"] == page1.get_url()
        assert item1.attrs["data-dropdown-target"] == "toggler"
        assert items[0].attrs["data-controller"] == "dropdown"
        submenu1_items = items[0].find("ul").find_all("li")
        assert len(submenu1_items) == 2
        assert (
            submenu1_items[0].find("a", href=subpage11.get_url()).text.strip()
            == subpage11.title
        )
        assert (
            submenu1_items[1].find("a", href=subpage12.get_url()).text.strip()
            == subpage12.title
        )

        item2 = items[1].find("a")
        assert item2.text.strip() == page2.title
        assert item2.attrs["href"] == page2.get_url()
        assert "data-dropdown-target" not in item2.attrs
        assert "data-controller" not in items[1].attrs
        assert not items[1].find("ul")

        item3 = items[2].find("a")
        assert item3.text.strip() == page3.title
        assert item3.attrs["href"] == page3.get_url()

    def test_footer_menu(self, home_page):
        url = home_page.get_url()

        page1 = IndexPageFactory(
            parent=home_page,
            title="Infos pratiques",
        )
        ContentPageFactory(parent=page1, title="Le parcours")
        ContentPageFactory(parent=page1, title="Hébergement")

        page2 = GuestBookPageFactory(parent=home_page)

        page3 = ContentPageFactory(parent=home_page, show_in_menus=False)

        PageNavigationFactory(page=page1)
        PageNavigationFactory(page=page1, menu="footer", order=1)
        PageNavigationFactory(page=page2, menu="footer", order=2)
        PageNavigationFactory(page=page3, menu="footer")

        html = self.get(url, status=200).html
        footer_menus = html.select("footer nav")
        assert len(footer_menus) == 1

        items = footer_menus[0].find_all("a", recursive=False)
        assert len(items) == 2
        assert items[0].text.strip() == page1.title
        assert items[1].text.strip() == page2.title


class TestHomePage(ViewMixin):
    def test_default(self, home_page):
        url = home_page.get_url()

        html = self.get(url, status=200).html
        assert html.select('[role="alert"] p')[0].text.strip() == (
            "Les inscriptions sont ouvertes !"
        )
        assert not html.select('[role="alert"] a.btn')
        background_img = html.select("main img")[0]
        assert background_img.attrs["alt"] == ""
        assert len(background_img.attrs["srcset"].split(",")) == 4

    def test_alert_with_button(self, home_page):
        url = home_page.get_url()

        home_page.alert_button_text = "Je m'inscris !"
        home_page.save()

        html = self.get(url, status=200).html
        assert html.select('[role="alert"]')
        assert not html.select('[role="alert"] a.btn')

        home_page.alert_button_link = home_page
        home_page.save()

        html = self.get(url, status=200).html
        alert_button = html.select('[role="alert"] a.btn')[0]
        assert alert_button.text.strip() == "Je m'inscris !"
        assert alert_button.attrs["href"] == url

    @pytest.mark.parametrize("home_page__is_alert_visible", [False])
    def test_no_alert(self, home_page):
        url = home_page.get_url()

        html = self.get(url, status=200).html
        assert not html.select('[role="alert"]')


class TestContentPage(ViewMixin):
    def test_render(self, root_page):
        page = ContentPageFactory(
            parent=root_page,
            title="Le parcours",
            introduction=RichText("<p>introduction</p>"),
        )
        url = page.get_url()

        html = self.get(url, status=200).html
        assert html.find("h2").text.strip() == "Le parcours"
        assert not html.find("p", string="introduction")

        page.show_introduction_in_header = True
        page.save()

        html = self.get(url, status=200).html
        assert html.find("p", string="introduction")

    def test_title_paragraph_blocks(self, root_page):
        page = ContentPageFactory(
            parent=root_page,
            body__0__title_block__value="Descriptions du parcours",
            body__1__paragraph_block__value=RichText(
                "<p>Lorem <b>ipsum</b> bla bla bla.</p>"
            ),
        )

        html = self.get(page.get_url(), status=200).html
        assert html.find("h3", id="descriptions-du-parcours").text.strip() == (
            "Descriptions du parcours"
        )
        assert str(html.select_one("main .prose > p")) == (
            "<p>Lorem <b>ipsum</b> bla bla bla.</p>"
        )

    def test_image_block(self, root_page):
        page = ContentPageFactory(
            parent=root_page,
            body__0__image_block__image=ImageFactory(),
            body__0__image_block__caption="Une légende",
            body__0__image_block__link={
                "type": "url",
                "url": "https://example.org",
            },
            body__1__image_block__image=ImageFactory(),
        )

        html = self.get(page.get_url(), status=200).html
        items = html.select("main figure")
        assert len(items) == 2
        assert items[0].find("img")
        assert items[0].find("figcaption").text.strip() == "Une légende"
        assert items[0].find("a", href="https://example.org")
        assert items[1].find("img")
        assert not items[1].find("figcaption")
        assert not items[1].find("a")

    def test_table_block(self, root_page):
        page = ContentPageFactory(
            parent=root_page,
            body__0__table_block__value={
                "columns": [
                    {"type": "date", "heading": "Date"},
                    {"type": "time", "heading": "Heure"},
                    {"type": "text", "heading": "Contenu"},
                ],
                "rows": [
                    {"values": ["2023-01-01", "09:00:00", "Ligne 1"]},
                    {"values": ["2023-12-31", "12:00:00", "Ligne 2\nSur 2"]},
                ],
            },
        )

        html = self.get(page.get_url(), status=200).html
        rows = html.select("main table tbody > tr")
        assert len(rows) == 2
        row1_cells = rows[0].find_all("td")
        assert len(row1_cells) == 3
        assert row1_cells[0].text.strip() == "01/01/2023"
        assert row1_cells[1].text.strip() == "9h00"
        assert str(row1_cells[2].find("p")) == "<p>Ligne 1</p>"
        row2_cells = rows[1].find_all("td")
        assert row2_cells[0].text.strip() == "31/12/2023"
        assert row2_cells[1].text.strip() == "12h00"
        assert str(row2_cells[2].find("p")) == "<p>Ligne 2<br/>Sur 2</p>"

    def test_table_headcount_cell_block(self, root_page):
        page = ContentPageFactory(
            parent=root_page,
            body__0__table_block__value={
                "columns": [
                    {"type": "text", "heading": "Horaires"},
                    {"type": "headcount", "heading": "Poste 1"},
                    {"type": "headcount", "heading": "Poste 2"},
                    {"type": "headcount", "heading": "Poste 3"},
                ],
                "rows": [
                    {
                        "values": [
                            "19h à 21h",
                            {"current": 0, "needed": 2, "details": ""},
                            {"current": 2, "needed": 2, "details": ""},
                            {"current": 3, "needed": 2, "details": ""},
                        ],
                    },
                    {
                        "values": [
                            "21h à 23h",
                            {"current": 0, "needed": 0, "details": ""},
                            {"current": 2, "needed": 0, "details": ""},
                            {"current": 2, "needed": 2, "details": "Du\ntexte"},
                        ],
                    },
                ],
            },
        )

        html = self.get(page.get_url(), status=200).html
        rows = html.select("main table tbody > tr")
        assert len(rows) == 2
        row1_cells = rows[0].find_all("td")
        assert len(row1_cells) == 4
        assert row1_cells[0].text.strip() == "19h à 21h"
        assert row1_cells[1].text.strip() == "0 / 2"
        assert "bg-red-200" in row1_cells[1].attrs["class"]
        assert row1_cells[2].text.strip() == "2 / 2"
        assert "bg-green-200" in row1_cells[2].attrs["class"]
        assert row1_cells[3].text.strip() == "3 / 2"
        assert "bg-yellow-200" in row1_cells[3].attrs["class"]
        row2_cells = rows[1].find_all("td")
        assert row2_cells[0].text.strip() == "21h à 23h"
        assert row2_cells[1].text.strip() == "0 / 0"
        assert "bg-green-200" not in row2_cells[1].attrs["class"]
        assert row2_cells[2].text.strip() == "2 / 0"
        assert "bg-yellow-200" in row2_cells[2].attrs["class"]
        assert str(row2_cells[3].find("p")) == "<p>Du<br/>texte</p>"
        assert "bg-green-200" in row2_cells[3].attrs["class"]

    def test_images_grid_block(self, root_page):
        page = ContentPageFactory(
            parent=root_page,
            body__0__images_grid_block__size=ImagesGridBlock.Sizes.LOGO,
            body__0__images_grid_block__images__0__image=ImageFactory(),
            body__0__images_grid_block__images__0__caption="Une légende",
            body__0__images_grid_block__images__0__link={
                "type": "url",
                "url": "https://example.org",
            },
            body__0__images_grid_block__images__1__image=ImageFactory(),
        )

        html = self.get(page.get_url(), status=200).html
        items = html.select("main ul.grid > li")
        assert len(items) == 2
        assert items[0].find("img")
        assert items[0].find("figcaption").text.strip() == "Une légende"
        assert items[0].find("a", href="https://example.org")
        assert items[1].find("img")
        assert not items[1].find("figcaption")
        assert not items[1].find("a")

    def test_alert_block(self, root_page):
        page = ContentPageFactory(
            parent=root_page,
            body__0__alert_block__message=RichText(
                "<p>Une <b>information</b> à mettre en avant.</p>"
            ),
            body__0__alert_block__level="info",
        )

        html = self.get(page.get_url(), status=200).html
        assert str(html.select_one('main [role="alert"].alert-info p')) == (
            "<p>Une <b>information</b> à mettre en avant.</p>"
        )

    def test_route_map_block(self, root_page):
        route = RouteFactory(loops=None)
        route_loops = [
            RouteLoopFactory(route=route, name="Boucle #2", sort_order=2),
            RouteLoopFactory(route=route, name="Boucle #1", sort_order=1),
            RouteLoopFactory(route=route, name="Boucle #3", sort_order=3),
        ]

        page = ContentPageFactory(
            parent=root_page,
            body__0__route_map_block__route__route=route,
        )

        html = self.get(page.get_url(), status=200).html
        controller = html.select_one('main [data-controller="route-map"]')
        assert json.loads(controller.attrs["data-route-map-loops-value"]) == [
            {
                "pk": 2,
                "name": "Boucle #1",
                "color": "#e91e63",
                "gpxFile": route_loops[1].gpx_file.url,
            },
            {
                "pk": 1,
                "name": "Boucle #2",
                "color": "#e91e63",
                "gpxFile": route_loops[0].gpx_file.url,
            },
            {
                "pk": 3,
                "name": "Boucle #3",
                "color": "#e91e63",
                "gpxFile": route_loops[2].gpx_file.url,
            },
        ]
        legend_items = controller.select("ul > li")
        assert len(legend_items) == 3
        assert legend_items[0].find("button").text.strip() == (
            "Boucle #1 (12,256 km)"
        )


class TestIndexPage(ViewMixin):
    def test_render(self, root_page):
        page = IndexPageFactory(
            parent=root_page,
            title="Infos pratiques",
        )
        subpage1 = ContentPageFactory(
            parent=page,
            title="Le parcours",
            introduction=RichText("<p>la page du parcours</p>"),
        )
        subpage2 = ContentPageFactory(
            parent=page,
            title="Hébergement",
            introduction=RichText("<p>la page des hébergements</p>"),
            banner_image=None,
            show_in_menus=False,
        )

        html = self.get(page.get_url(), status=200).html
        items = html.select("main article")
        assert len(items) == 2

        assert items[0].find("img")
        assert items[0].find("h3").text.strip() == "Le parcours"
        assert items[0].find("a", href=subpage1.get_url())
        assert items[0].find("p", string="la page du parcours")

        assert not items[1].find("img")
        assert items[1].find("h3").text.strip() == "Hébergement"
        assert items[1].find("a", href=subpage2.get_url())
        assert items[1].find("p", string="la page des hébergements")
