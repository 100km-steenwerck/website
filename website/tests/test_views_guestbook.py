import datetime

import pytest
import time_machine

from .utils import ViewMixin


class TestGuestBookPage(ViewMixin):
    def test_render_no_entry(self, guest_book_page):
        url = guest_book_page.get_url()

        html = self.get(url, status=200).html
        assert not html.find(id="messages")
        assert html.find("p", string="Il n'y a aucun message pour le moment.")
        assert not html.find("nav", attrs={"aria-label": "Changement de page"})

    @pytest.mark.parametrize("guest_book_page__per_page", [2])
    def test_render_entries(self, guest_book_page, guest_book_entry_factory):
        entries = guest_book_entry_factory.create_batch(3)
        invisible_entry = guest_book_entry_factory(is_visible=False)

        url = guest_book_page.get_url()

        html = self.get(url, status=200).html
        assert len(html.select("#messages > *")) == 2
        assert not html.find(id="message-%d" % invisible_entry.id)
        assert html.find(id="message-%d" % entries[2].id)
        assert html.find(id="message-%d" % entries[1].id)
        assert html.find("nav", attrs={"aria-label": "Changement de page"})

        html = self.get("%s?p=2" % url, status=200).html
        assert len(html.select("#messages > *")) == 1
        assert html.find(id="message-%d" % entries[0].id)

    @time_machine.travel(datetime.date(2023, 11, 17))
    def test_add_entry(self, guest_book_page):
        page_url = guest_book_page.get_url()
        url = page_url + guest_book_page.reverse_subpage("add_entry")

        response = self.get(url, status=200)
        assert "entries" not in response.context

        form = response.form
        form["name"] = "Camille"
        response = form.submit(status=200)

        form["message"] = "Coucou,\nVoici mon message."
        response = form.submit(status=302)
        assert response["location"] == page_url
        html = response.follow().html
        assert len(html.select("#messages > *")) == 1
        assert html.select("#messages p")[0].text.strip() == "Camille"
        assert html.select("#messages time")[0].text.strip() == (
            "17 novembre 2023"
        )
        assert "Coucou,<br/>Voici" in str(html.select("#messages p")[1])
