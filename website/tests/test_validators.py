from io import BytesIO

from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.core.files.images import ImageFile

import PIL.Image
import pytest

from website.validators import FileSizeValidator, FileTypeValidator


@pytest.fixture
def jpeg_image():
    f = BytesIO()
    image = PIL.Image.new("RGB", (100, 100), "red")
    image.save(f, "JPEG")
    return ImageFile(f, name="image.jpg")


class TestFileSizeValidator:
    def test_eq(self):
        assert FileSizeValidator(10) == FileSizeValidator(10)
        assert FileSizeValidator(10) != FileSizeValidator(1)

    def test_valid(self):
        file = ContentFile("contents", name="file.txt")

        FileSizeValidator()(file)
        FileSizeValidator(10)(file)

    def test_too_large(self):
        file = ContentFile("a" * 1025, name="file.txt")

        with pytest.raises(ValidationError) as excinfo:
            FileSizeValidator(1024)(file)
        assert excinfo.value.message == (
            "La taille du fichier (1,0\xa0Kio) dépasse celle autorisée "
            "(maximum : 1,0\xa0Kio)."
        )


class TestFileTypeValidator:
    def test_eq(self):
        assert FileTypeValidator(["a"]) == FileTypeValidator(["a"])
        assert FileTypeValidator(["a"]) != FileTypeValidator(["b"])

    def test_valid(self, jpeg_image):
        FileTypeValidator()(jpeg_image)
        FileTypeValidator(["image/jpeg"])(jpeg_image)

    def test_invalid_type(self, jpeg_image):
        with pytest.raises(ValidationError) as excinfo:
            FileTypeValidator(["application/pdf"])(jpeg_image)
        assert excinfo.value.message == (
            "Le format du fichier (image/jpeg) n'est pas autorisé "
            "(types autorisés : application/pdf)."
        )

    def test_unknown_type(self):
        file = ContentFile(b"somedata", name="file.txt")

        with pytest.raises(ValidationError) as excinfo:
            FileTypeValidator(["application/pdf"])(file)
        assert excinfo.value.message == (
            "Le format du fichier (inconnu) n'est pas autorisé "
            "(types autorisés : application/pdf)."
        )
