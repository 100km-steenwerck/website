import datetime

import pytest
import time_machine

from .utils import ViewMixin


class TestRaceIndexPage(ViewMixin):
    @time_machine.travel(datetime.date(2024, 3, 1))
    def test_render(
        self,
        race_index_page,
        race_page,
        open2024_race_page,
        open2024_race_edition,
    ):
        html = self.get(race_index_page.url, status=200).html
        items = html.select("main article")
        assert len(items) == 2

        assert items[0].find("h3").text.strip() == race_page.title
        assert items[0].find("a", href=race_page.url)
        assert not items[0].find("a", string="S'inscrire")

        assert items[1].find("h3").text.strip() == "100 km Open 24h"
        assert items[1].find("a", href=open2024_race_page.url)
        assert items[1].find("a", string="S'inscrire").attrs["href"] == (
            open2024_race_page.url
            + f"edition/{open2024_race_edition.issue}/register/"
        )

    @time_machine.travel(datetime.date(2024, 2, 1))
    def test_registration_closed(self, race_index_page, open2024_race_edition):
        html = self.get(race_index_page.url, status=200).html
        items = html.select("main article")
        assert len(items) == 1
        assert not items[0].find("a", string="S'inscrire")


class TestRacePage(ViewMixin):
    @time_machine.travel(datetime.date(2024, 3, 1))
    def test_registration_opened(
        self, open2024_race_page, open2024_race_edition
    ):
        html = self.get(open2024_race_page.url, status=200).html
        dl = html.select_one("main dl")

        items = dl.find_all("div", recursive=False)
        assert len(items) == 4
        assert items[0].find("dd").text.strip() == (
            "Mercredi 8 mai 2024 à 19:00"
        )
        assert items[1].find("dd").text.strip() == "100 km"
        assert items[2].find("dd").text.strip() == "30 €"
        assert items[3].find("dd").text.strip() == "18 ans"

        assert dl.find_next("a", string="S'inscrire").attrs["href"] == (
            open2024_race_page.url
            + f"edition/{open2024_race_edition.issue}/register/"
        )

    @time_machine.travel(datetime.date(2024, 5, 2))
    def test_registration_ended(
        self, open2024_race_page, open2024_race_edition
    ):
        html = self.get(open2024_race_page.url, status=200).html
        dl = html.select_one("main dl")

        assert dl.find_next("p").text.strip() == (
            "Les inscriptions sont terminées."
        )

    @time_machine.travel(datetime.date(2024, 2, 1))
    def test_registration_not_yet_opened(
        self, open2024_race_page, open2024_race_edition
    ):
        html = self.get(open2024_race_page.url, status=200).html
        dl = html.select_one("main dl")

        assert dl.find_next("p").text.strip() == (
            "Les inscriptions ne sont pas encore ouvertes."
        )

    @time_machine.travel(datetime.date(2024, 3, 1))
    @pytest.mark.parametrize("open2024_race_edition__has_registration", [False])
    def test_no_registration(self, open2024_race_page, open2024_race_edition):
        html = self.get(open2024_race_page.url, status=200).html
        dl = html.select_one("main dl")

        items = dl.find_all("div", recursive=False)
        assert len(items) == 2
        assert items[0].find("dd").text.strip() == (
            "Mercredi 8 mai 2024 à 19:00"
        )
        assert items[1].find("dd").text.strip() == "100 km"

        assert not dl.find_next("p")
        assert not dl.find_next("a", string="S'inscrire")
