"""
Django and project dependencies settings.

For more information on this file, see
https://docs.djangoproject.com/en/stable/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/stable/ref/settings/
"""

import os.path
from email.utils import parseaddr
from urllib.parse import urlparse

from . import base_dir, env, root_dir

# ENVIRONMENT VARIABLES AND PATHS
# ------------------------------------------------------------------------------

# Local directory used for static and templates overrides
local_dir = base_dir.path("local")

# Directory for variable stuffs, i.e. user-uploaded media
var_dir = base_dir.path("var")
if not os.path.isdir(var_dir()):
    os.mkdir(var_dir(), mode=0o755)

# Base URL on which the application is served without trailing slash
BASE_URL = env("BASE_URL", default="http://127.0.0.1:8000")

# Location on which the application is served
APP_PATH = urlparse(BASE_URL).path or "/"

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#debug
DEBUG = env.bool("DJANGO_DEBUG", default=True)

# Local time zone for this installation
TIME_ZONE = "Europe/Paris"

# https://docs.djangoproject.com/en/stable/ref/settings/#language-code
LANGUAGE_CODE = "fr"

# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# https://docs.djangoproject.com/en/stable/ref/settings/#use-tz
USE_TZ = True

# Django sets a maximum of 1000 fields per form by default, but particularly
# complex page models can exceed this limit within Wagtail's page editor
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10_000

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#databases
# https://django-environ.readthedocs.io/en/stable/#supported-types
DATABASES = {
    "default": env.db(
        "DJANGO_DATABASE_URL",
        default="sqlite:///{}".format(base_dir("sqlite.db")),
    )
}

# https://docs.djangoproject.com/en/stable/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#root-urlconf
ROOT_URLCONF = "website.urls"
# https://docs.djangoproject.com/en/stable/ref/settings/#wsgi-application
WSGI_APPLICATION = "website.wsgi.application"

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_cleanup.apps.CleanupConfig",
]

WAGTAIL_APPS = [
    "wagtail.contrib.forms",
    "wagtail.contrib.redirects",
    "wagtail.contrib.routable_page",
    "wagtail.contrib.settings",
    "wagtail.contrib.typed_table_block",
    "wagtail.embeds",
    "wagtail.sites",
    "wagtail.users",
    "wagtail.snippets",
    "wagtail.documents",
    "wagtail.images",
    "wagtail.search",
    "wagtail.admin",
    "wagtail",
    "modelcluster",
    "taggit",
]

THIRD_PARTY_APPS = ["tapeforms"]

LOCAL_APPS = ["website", "website.admin"]

# https://docs.djangoproject.com/en/stable/ref/settings/#installed-apps
INSTALLED_APPS = LOCAL_APPS + THIRD_PARTY_APPS + WAGTAIL_APPS + DJANGO_APPS

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#auth
AUTH_USER_MODEL = "website.User"

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/topics/auth/passwords/#password-validation
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": (
            "django.contrib.auth.password_validation."
            "UserAttributeSimilarityValidator"
        )
    },
    {
        "NAME": (
            "django.contrib.auth.password_validation.MinimumLengthValidator"
        )
    },
    {
        "NAME": (
            "django.contrib.auth.password_validation.CommonPasswordValidator"
        )
    },
    {
        "NAME": (
            "django.contrib.auth.password_validation.NumericPasswordValidator"
        )
    },
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#middleware
MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "wagtail.contrib.redirects.middleware.RedirectMiddleware",
]

# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#static-files
STATIC_ROOT = var_dir("static")

# https://docs.djangoproject.com/en/stable/ref/settings/#static-url
STATIC_URL = os.path.join(APP_PATH, "static/")

# https://docs.djangoproject.com/en/stable/ref/settings/#staticfiles-dirs
STATICFILES_DIRS = [root_dir("static")]
if os.path.isdir(local_dir("static")):
    STATICFILES_DIRS.insert(0, local_dir("static"))

# https://docs.djangoproject.com/en/stable/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#media-root
MEDIA_ROOT = var_dir("media")

# https://docs.djangoproject.com/en/stable/ref/settings/#media-url
MEDIA_URL = os.path.join(APP_PATH, "media/")

# https://docs.djangoproject.com/en/stable/ref/settings/#file-upload-directory-permissions
FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0o755
# https://docs.djangoproject.com/en/stable/ref/settings/#file-upload-permissions
FILE_UPLOAD_PERMISSIONS = 0o644

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#templates
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [root_dir("templates")],
        "OPTIONS": {
            "debug": DEBUG,
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "website.context_processors.root_page",
                "website.context_processors.site_settings",
            ],
            "builtins": [
                "website.templatetags.builtins",
            ],
        },
    }
]
if os.path.isdir(local_dir("templates")):
    TEMPLATES[0]["DIRS"].insert(0, local_dir("templates"))

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/topics/email/#email-backends
# https://django-environ.readthedocs.io/en/stable/#supported-types
vars().update(env.email_url("DJANGO_EMAIL_URL", default="smtp://localhost:25"))

DEFAULT_FROM_EMAIL = env("DEFAULT_FROM_EMAIL", default="webmaster@localhost")

# Use the same email address for error messages
SERVER_EMAIL = DEFAULT_FROM_EMAIL

# ADMIN
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#admins
ADMINS = tuple(parseaddr(email) for email in env.list("ADMINS", default=[]))

# https://docs.djangoproject.com/en/stable/ref/settings/#managers
MANAGERS = ADMINS

# SESSIONS AND COOKIES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/stable/ref/settings/#session-cookie-path
SESSION_COOKIE_PATH = APP_PATH

# https://docs.djangoproject.com/en/stable/ref/settings/#csrf-cookie-path
CSRF_COOKIE_PATH = APP_PATH

# WAGTAIL
# ------------------------------------------------------------------------------
# http://docs.wagtail.io/en/stable/advanced_topics/settings.html
WAGTAIL_SITE_NAME = "100km à pied de Steenwerck"

# Disable Gravatar provider
WAGTAIL_GRAVATAR_PROVIDER_URL = None

# Disable update checking on the dashboard
WAGTAIL_ENABLE_UPDATE_CHECK = False

# Disable the dashboard banner
WAGTAIL_ENABLE_WHATS_NEW_BANNER = False

# Disable moderation workflows
WAGTAIL_WORKFLOW_ENABLED = False

# Limit slugs to ASCII characters
WAGTAIL_ALLOW_UNICODE_SLUGS = False

# Disable commenting
WAGTAILADMIN_COMMENTS_ENABLED = False

# Configure search backend for PostgreSQL
WAGTAILSEARCH_BACKENDS = {
    "default": {
        "BACKEND": "wagtail.search.backends.database",
        "SEARCH_CONFIG": "french",
    }
}

WAGTAILADMIN_BASE_URL = BASE_URL

# ------------------------------------------------------------------------------
# APPLICATION AND 3RD PARTY LIBRARY SETTINGS
# ------------------------------------------------------------------------------

# Email address to use in the "Reply-To" header for correspondence related to
# registrations
REGISTRATION_REPLY_TO = env.list("REGISTRATION_REPLY_TO", default=None)

# Whether to use the production environment of HelloAsso API
USE_HELLOASSO_PRODUCTION = env.bool("USE_HELLOASSO_PRODUCTION", default=False)

# DJANGO PHONENUMBER FIELD
# ------------------------------------------------------------------------------
# https://django-phonenumber-field.readthedocs.io/en/stable/reference.html
PHONENUMBER_DEFAULT_REGION = "FR"
