from django import template

from ..models import NavigationMenu

register = template.Library()


def is_page_active(page, current_page=None):
    """
    Détermine si la page `page` est considérée active en fonction de la page
    actuelle `current_page`, c'est-à-dire si c'est la même ou si c'est une
    sous-page de celle-ci.
    """
    return (
        current_page.url_path.startswith(page.url_path)
        if current_page
        else False
    )


def is_page_current(page, current_page=None):
    """
    Détermine si la page `page` est la page actuelle `current_page`.
    """
    return page.url_path == current_page.url_path if current_page else False


def get_navigation_menu_items(
    navigation_menu,
    root_page,
    current_page=None,
    with_submenu=False,
):
    """
    Retourne les pages du menu `navigation_menu` ayant pour racine `root_page`,
    en définissant leur état en fonction de la page actuelle `current_page` et
    avec leur sous-menu si `with_submenu` vaut `True`.
    """
    if hasattr(root_page, "navigation_menu_items"):
        if navigation_menu in root_page.navigation_menu_items:
            return root_page.navigation_menu_items[navigation_menu]
    else:
        root_page.navigation_menu_items = {}

    menu_items = (
        root_page.get_descendants()
        .live()
        .in_menu()
        .filter(navigation__menu=navigation_menu)
        .order_by("navigation__order")
        .specific(defer=True)
    )

    for page in menu_items:
        page.is_active = is_page_active(page, current_page)
        page.is_current = is_page_current(page, current_page)

        if with_submenu:
            page.children = page.get_children().live().in_menu()
            page.has_children = len(page.children) > 0

            for child in page.children:
                child.is_active = is_page_active(child, current_page)
                child.is_current = is_page_current(child, current_page)

    # Mets en cache dans la page racine les éléments de ce menu
    root_page.navigation_menu_items[navigation_menu] = menu_items

    return menu_items


@register.inclusion_tag(
    "tags/main_menu.html",
    name="main_menu",
    takes_context=True,
)
@register.inclusion_tag(
    "tags/main_menu_mobile.html",
    name="main_menu_mobile",
    takes_context=True,
)
def main_menu(context, root_page, current_page=None):
    """
    Retourne les élements du menu principal présenté sur deux niveaux, avec,
    pour chaque page, son éventuel sous-menu et si celle-ci est active - en
    fonction de la page actuelle `current_page`.
    """
    return {
        "items": get_navigation_menu_items(
            NavigationMenu.MAIN, root_page, current_page, with_submenu=True
        ),
        "request": context["request"],
    }


@register.inclusion_tag("tags/footer_menu.html", takes_context=True)
def footer_menu(context, root_page, current_page=None):
    """
    Retourne les éléments du menu affiché en pied de page sur un seul niveau,
    avec, pour chaque page, si celle-ci est active - en fonction de la page
    actuelle `current_page`.
    """
    return {
        "items": get_navigation_menu_items(
            NavigationMenu.FOOTER, root_page, current_page
        ),
        "request": context["request"],
    }
