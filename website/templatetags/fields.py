from pathlib import Path

from django import template

register = template.Library()


@register.inclusion_tag("tags/icon.html", takes_context=False)
def icon(name, **kwargs):
    kwargs["name"] = name
    return kwargs


@register.filter
def filename(value):
    return Path(value).name
