from django import template

from wagtail.admin.templatetags.wagtailadmin_tags import BlockInclusionNode

register = template.Library()


@register.inclusion_tag("tags/icon.html", takes_context=False)
def icon(name, **kwargs):
    kwargs["name"] = name
    return kwargs


class AlertNode(BlockInclusionNode):
    template = "tags/alert.html"


register.tag("alert", AlertNode.handle)


class FieldsetNode(BlockInclusionNode):
    template = "tags/fieldset.html"


register.tag("fieldset", FieldsetNode.handle)


class PageDividerNode(BlockInclusionNode):
    template = "tags/page_divider.html"


register.tag("pagedivider", PageDividerNode.handle)
