from django.conf import settings
from django.urls import include, path

from wagtail import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls

urlpatterns = [
    path("admin/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
]

handler400 = "website.views.errors.bad_request"
handler403 = "website.views.errors.permission_denied"
handler404 = "website.views.errors.page_not_found"

if settings.DEBUG:  # pragma: no cover
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.views import defaults as default_views

    from .views import errors as error_views

    # Sers les médias et les fichiers statiques
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # Sers les pages d'erreurs afin de les mettre en forme
    urlpatterns += [
        path(
            "400/",
            error_views.bad_request,
            kwargs={"exception": Exception("Un message d'erreur à afficher.")},
        ),
        path(
            "403/",
            error_views.permission_denied,
            kwargs={"exception": Exception("Un message d'erreur à afficher.")},
        ),
        path(
            "404/",
            error_views.page_not_found,
            kwargs={"exception": Exception("Un message d'erreur à afficher.")},
        ),
        path("500/", default_views.server_error),
    ]

    if "debug_toolbar" in settings.INSTALLED_APPS:
        urlpatterns.insert(0, path("__debug__/", include("debug_toolbar.urls")))

urlpatterns = urlpatterns + [
    # Pour tout ce qui n'est pas attrapé jusque-là par une règle plus
    # spécifique, passe le relais au mécanisme des pages Wagtail
    path("", include(wagtail_urls)),
]
