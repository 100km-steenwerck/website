export default {
  plugins: ['prettier-plugin-tailwindcss'],
  arrowParens: 'always',
  quoteProps: 'consistent',
  semi: true,
  singleQuote: true,
};
