const colors = require('./assets/colors');

export default {
  content: [
    './assets/js/**/*.js',
    './website/**/*.{py,html}',
  ],
  theme: {
    colors: {
      inherit: 'inherit',
      current: 'currentColor',
      transparent: 'transparent',
      white: '#fff',
      black: '#000',
      gray: colors.gray,
      primary: {
        DEFAULT: colors.brandBlue['800'],
        ...colors.brandBlue,
      },
      secondary: {
        DEFAULT: colors.brandYellow['500'],
        ...colors.brandYellow,
      },
      red: {
        DEFAULT: colors.red['600'],
        ...colors.red,
      },
      green: {
        DEFAULT: colors.green['600'],
        ...colors.green,
      },
      blue: {
        DEFAULT: colors.blue['500'],
        ...colors.blue,
      },
      yellow: {
        DEFAULT: colors.yellow['500'],
        ...colors.yellow,
      },
    },
    fontFamily: {
      body: ['Fira Sans', 'system-ui', 'sans-serif'],
      heading: ['Dosis', 'system-ui', 'sans-serif'],
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1440px',
    },
    extend: {
      typography: ({ theme }) => ({
        DEFAULT: {
          css: {
            '--tw-prose-body': theme('colors.gray[900]'),
            '--tw-prose-lead': theme('colors.gray[900]'),
            '--tw-prose-links': theme('colors.primary[700]'),
            'maxWidth': 'none',
            'a': {
              'fontWeight': theme('fontWeight.normal'),
              'textUnderlineOffset': '2px',
              '&:hover': {
                color: theme('colors.primary[900]'),
              },
            },
            '[class~="lead"]': {
              fontWeight: theme('fontWeight.medium'),
            },
          },
        },
      }),
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
};
