import globals from 'globals';
import xo from 'eslint-config-xo/space/browser';

export default [
  ...xo,
  {
    rules: {
      '@stylistic/arrow-parens': ['error', 'always'],
      '@stylistic/function-paren-newline': ['error', 'multiline-arguments'],
      '@stylistic/object-curly-spacing': ['error', 'always'],
      '@stylistic/operator-linebreak': [
        'error',
        'after',
        { overrides: { '?': 'before', ':': 'before' } },
      ],
      '@stylistic/quote-props': ['error', 'consistent-as-needed'],
    },
  },
  {
    files: [
      'tailwind.config.js',
      'webpack.config.js',
    ],
    languageOptions: {
      globals: {
        ...globals.node,
      },
    },
  },
];
