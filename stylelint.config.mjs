export default {
  extends: [
    'stylelint-config-standard',
  ],
  plugins: [
    'stylelint-declaration-strict-value',
    'stylelint-order',
  ],
  rules: {
    'at-rule-empty-line-before': null,
    'at-rule-no-unknown': [
      true,
      { ignoreAtRules: ['apply', 'layer'] },
    ],
    'block-no-empty': true,
    'color-hex-length': 'short',
    'color-named': 'never',
    'comment-no-empty': true,
    'declaration-block-no-duplicate-properties': true,
    'declaration-block-no-redundant-longhand-properties': true,
    'declaration-block-single-line-max-declarations': 1,
    'declaration-property-value-allowed-list': {
      // Only allow logical values, and resets.
      'clear': ['both', 'none'],
      'float': ['inline-start', 'inline-end', 'none', 'unset'],
      // Only allow logical values.
      'text-align': ['start', 'end', 'center', 'inherit'],
    },
    'declaration-property-value-disallowed-list': [
      { '/^border/': ['none'] },
      { severity: 'error' },
    ],
    'declaration-no-important': true,
    'font-family-no-duplicate-names': true,
    'function-calc-no-unspaced-operator': true,
    'function-linear-gradient-no-nonstandard-direction': true,
    'function-no-unknown': [
      true,
      { ignoreFunctions: ['theme'] },
    ],
    'function-url-quotes': 'always',
    'import-notation': 'string',
    'length-zero-no-unit': true,
    'max-nesting-depth': 3,
    'media-feature-name-no-unknown': true,
    'no-empty-source': true,
    'order/order': [
      {
        type: 'at-rule',
        name: 'apply',
      },
      'custom-properties',
      'declarations',
    ],
    'property-disallowed-list': [
      // Disallow positioning with physical properties, use logical ones instead.
      '/left/',
      '/right/',
    ],
    'property-no-unknown': true,
    'property-no-vendor-prefix': true,
    'rule-empty-line-before': [
      'always',
      { except: ['after-single-line-comment', 'first-nested'] },
    ],
    'scale-unlimited/declaration-strict-value': [
      [
        // Use consistent values from Tailwind CSS theme for:
        // - colors
        '/color/',
        'fill',
        'stroke',
        // - font related
        'font-family',
        'font-size',
        'font-weight',
        // - spacing
        '/margin/',
        '/padding/',
        'gap',
        // - z-index
        'z-index',
      ],
      {
        ignoreValues: [
          'currentcolor',
          'inherit',
          'initial',
          'none',
          'unset',
          'transparent',
          '0',
        ],
      },
    ],
    'selector-attribute-name-disallowed-list': '/^data-/',
    'selector-class-pattern': [
      // Loose pattern for hyphenated BEM. This also allows simple words to be used as class names, .e.g. `.active`, `.button`.
      // Based on:
      // - https://github.com/postcss/postcss-bem-linter/issues/89#issuecomment-255482072
      // - https://gist.github.com/Potherca/f2a65491e63338659c3a0d2b07eee382
      // - https://github.com/torchbox/stylelint-config-torchbox/blob/2d1dffc2d6af49d1327e66daf51d520dd50c5fdc/config.js#L22-L31
      // See also: https://github.com/simonsmith/stylelint-selector-bem-pattern.
      // Proceed with caution if reviewing this – and use https://regexr.com/
      /^[a-z]+[0-9]{0,2}(-[a-z0-9]+)*(__[a-z0-9]+(-[a-z0-9]+)*)?(--[a-z0-9]+(-[a-z0-9]+)*)?$/,
      { resolveNestedSelectors: true },
    ],
    'selector-max-combinators': 3,
    'selector-max-id': 0,
    'selector-max-specificity': '0,3,3',
    'selector-no-qualifying-type': [true, { ignore: ['attribute', 'class'] }],
    'selector-pseudo-element-no-unknown': true,
    'selector-type-no-unknown': true,
    'string-no-newline': true,
    'unit-no-unknown': true,
    'value-keyword-case': [
      'lower',
      { ignoreFunctions: ['theme'] },
    ],
    'value-no-vendor-prefix': true,
  },
};
