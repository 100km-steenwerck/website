# Journal des modifications

Tous les changements importants sont documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/) et le
versionnement de l'application suit le [Semantic Versioning](http://semver.org/).

## [Unreleased]
